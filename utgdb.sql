--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: postgres; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON DATABASE postgres IS 'default administrative connection database';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: adminpack; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION adminpack; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: Strategy; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE "Strategy" (
    "Strategy" character varying(255) NOT NULL
);


--
-- Name: agent_details; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE agent_details (
    agent bigint,
    tier_level bigint,
    tier_posotion bigint,
    queue_name character varying(255)
);


--
-- Name: audio_file; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE audio_file (
    id integer NOT NULL,
    name character varying(150) NOT NULL,
    audio_file character varying(100) NOT NULL
);


--
-- Name: audio_file_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE audio_file_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: audio_file_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE audio_file_id_seq OWNED BY audio_file.id;


--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL,
    is_user boolean,
    is_admin boolean,
    is_agent boolean,
    is_enabled boolean,
    user_group integer,
    domain character varying(254),
    outbound_caller_id bigint,
    mobile bigint,
    call_queue bigint,
    username bigint
);


--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE auth_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE auth_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE auth_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;


--
-- Name: blaclisted_tables; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE blaclisted_tables (
    id bigint,
    "user" character varying(245),
    description character varying(255)
);


--
-- Name: call_logs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE call_logs (
    id bigint NOT NULL,
    date_time date,
    "from" bigint,
    "to" bigint
);


--
-- Name: call_queue_userseq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE call_queue_userseq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: call_queue_users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE call_queue_users (
    call_queue bigint,
    "user" bigint,
    id bigint DEFAULT nextval('call_queue_userseq'::regclass) NOT NULL
);


--
-- Name: call_queues; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE call_queues (
    id bigint NOT NULL,
    extension bigint,
    poll_strategy character varying(255),
    timeout bigint,
    queue_name character varying(255)
);


--
-- Name: cdr; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE cdr (
    id integer NOT NULL,
    customer character varying(100),
    customer_ip character varying(100),
    uuid character varying(100),
    bleg_uuid character varying(100),
    caller_id_number character varying(100),
    destination_number character varying(100),
    chan_name character varying(100),
    start_stamp timestamp with time zone,
    answered_stamp timestamp with time zone,
    end_stamp timestamp with time zone,
    duration integer,
    effectiv_duration integer,
    effective_duration integer,
    billsec integer,
    read_codec character varying(20),
    write_codec character varying(20),
    hangup_cause character varying(50),
    hangup_cause_q850 integer,
    cost_rate numeric(11,5),
    total_sell numeric(11,5),
    total_cost numeric(11,5),
    prefix character varying(30),
    rate numeric(11,5),
    init_block numeric(11,5),
    block_min_duration integer,
    sip_user_agent character varying(100),
    sip_rtp_rxstat character varying(30),
    sip_rtp_txstat character varying(30),
    switchname character varying(100),
    switch_ipv4 character varying(100),
    hangup_disposition character varying(100),
    sip_hangup_cause character varying(100),
    sell_destination character varying(128),
    cost_destination character varying(128),
    gateway_id integer
);


--
-- Name: cdr_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE cdr_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: cdr_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE cdr_id_seq OWNED BY cdr.id;


--
-- Name: chat_log; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE chat_log (
    id bigint NOT NULL,
    "from" bigint,
    "to" bigint,
    message character varying
);


--
-- Name: contacts; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE contacts (
    id bigint NOT NULL,
    first_name character varying(255),
    last_name character varying(255),
    company character varying(255),
    business character varying(255),
    email character varying(255),
    mobile bigint
);


--
-- Name: crm; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE crm (
    id bigint NOT NULL,
    crm_name character varying(255),
    contact_match_link character varying(2500),
    open_contact_link character varying(2500),
    create_contact_link character varying(2500),
    journeling_link character varying(2500)
);


--
-- Name: date_dimension; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE date_dimension (
    id integer NOT NULL,
    date timestamp with time zone NOT NULL,
    day character varying(2) NOT NULL,
    day_of_week character varying(30) NOT NULL,
    hour character varying(2),
    month character varying(2) NOT NULL,
    quarter character varying(2) NOT NULL,
    year character varying(4) NOT NULL
);


--
-- Name: date_dimension_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE date_dimension_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: date_dimension_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE date_dimension_id_seq OWNED BY date_dimension.id;


--
-- Name: devices; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE devices (
    id bigint NOT NULL,
    extension bigint,
    device_model character varying(255),
    name character varying(255),
    user_id bigint,
    password character varying(255),
    ip_address character varying(255),
    mac_address character varying(255)
);


--
-- Name: dialplan; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE dialplan (
    id bigint,
    name character varying(255),
    condition1 character varying(255),
    condition2 character varying(255),
    action1 character varying(255),
    action2 character varying(255),
    "order" bigint,
    enabled boolean,
    description character varying(255),
    value1 character varying(255),
    value2 character varying(255),
    action_value1 character varying(255),
    action_value2 character varying(255)
);


--
-- Name: dialplan_actions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE dialplan_actions (
    id bigint,
    action character varying(255)
);


--
-- Name: dialplan_conditions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE dialplan_conditions (
    id bigint,
    condition character varying(255)
);


--
-- Name: digital_actions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE digital_actions (
    action character varying NOT NULL
);


--
-- Name: digital_receptionist; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE digital_receptionist (
    name character varying(255),
    prompt character varying(255),
    extension bigint,
    action_0 character varying(255),
    action_1 character varying(255),
    action_2 character varying(255),
    action_3 character varying(255),
    action_4 character varying(255),
    action_5 character varying(255),
    action_6 character varying(255),
    action_7 character varying(255),
    action_8 character varying(255),
    action_9 character varying(255),
    ac_value_0 character varying(255),
    ac_value_1 character varying(255),
    ac_value_2 character varying(255),
    ac_value_3 character varying(255),
    ac_value_4 character varying(255),
    ac_value_5 character varying(255),
    ac_value_6 character varying(255),
    ac_value_7 character varying(255),
    ac_value_8 character varying(255),
    ac_value_9 character varying(255),
    inactive_second bigint,
    inactive_action character varying(255),
    invalid_action character varying(255),
    id bigint NOT NULL
);


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE django_migrations_id_seq OWNED BY django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


--
-- Name: domains; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE domains (
    id bigint NOT NULL,
    domain_name character varying(254),
    is_enabled boolean,
    description character varying(254),
    max_connections bigint
);


--
-- Name: email_setup; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE email_setup (
    smtp_host character varying(255) NOT NULL,
    port bigint,
    user_name character varying(255),
    password character varying(255),
    template_name character varying(255),
    from_field character varying(255),
    subject character varying(500),
    body character varying(5000)
);


--
-- Name: hangup_cause; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE hangup_cause (
    id integer NOT NULL,
    code integer NOT NULL,
    enumeration character varying(100),
    cause character varying(100),
    description text NOT NULL,
    date_added timestamp with time zone NOT NULL,
    date_modified timestamp with time zone NOT NULL,
    CONSTRAINT hangup_cause_code_check CHECK ((code >= 0))
);


--
-- Name: hangup_cause_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE hangup_cause_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: hangup_cause_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE hangup_cause_id_seq OWNED BY hangup_cause.id;


--
-- Name: inboundrules; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE inboundrules (
    id bigint NOT NULL,
    type character varying(255),
    name character varying(255),
    trunk character varying(255),
    did_number bigint,
    in_office_routing bigint,
    out_office_routing bigint
);


--
-- Name: outbound_rules; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE outbound_rules (
    id bigint NOT NULL,
    name character varying(255),
    call_from bigint,
    prefix bigint,
    ext_group bigint,
    route1 bigint,
    route2 bigint,
    route3 bigint,
    route4 bigint,
    route5 bigint
);


--
-- Name: phone_vendors; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE phone_vendors (
    id bigint NOT NULL,
    vendor character varying,
    model character varying(255),
    version character varying(255)
);


--
-- Name: ring_groups; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE ring_groups (
    id bigint NOT NULL,
    extension bigint,
    name character varying(255),
    type character varying(255),
    if_no_ans character varying(255)
);


--
-- Name: ringgroup_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE ringgroup_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ringgroup_strategy; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE ringgroup_strategy (
    name character varying(255) NOT NULL
);


--
-- Name: ringgroup_user; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE ringgroup_user (
    ringgroup_id bigint,
    user_id bigint,
    id bigint DEFAULT nextval('ringgroup_seq'::regclass) NOT NULL
);


--
-- Name: sip_profile; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE sip_profile (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    user_agent character varying(50) NOT NULL,
    ext_rtp_ip character varying(100) NOT NULL,
    ext_sip_ip character varying(100) NOT NULL,
    rtp_ip character varying(100) NOT NULL,
    sip_ip character varying(100) NOT NULL,
    sip_port integer NOT NULL,
    disable_transcoding boolean NOT NULL,
    accept_blind_reg boolean NOT NULL,
    disable_register boolean NOT NULL,
    apply_inbound_acl boolean NOT NULL,
    auth_calls boolean NOT NULL,
    log_auth_failures boolean NOT NULL,
    inbound_codec_prefs character varying(100) NOT NULL,
    outbound_codec_prefs character varying(100) NOT NULL,
    aggressive_nat_detection boolean NOT NULL,
    "NDLB_rec_in_nat_reg_c" boolean NOT NULL,
    "NDLB_force_rport" character varying(10),
    "NDLB_broken_auth_hash" boolean NOT NULL,
    enable_timer boolean NOT NULL,
    session_timeout integer NOT NULL,
    rtp_rewrite_timestamps boolean NOT NULL,
    pass_rfc2833 boolean NOT NULL,
    date_added timestamp with time zone NOT NULL,
    date_modified timestamp with time zone NOT NULL,
    CONSTRAINT sip_profile_session_timeout_check CHECK ((session_timeout >= 0)),
    CONSTRAINT sip_profile_sip_port_check CHECK ((sip_port >= 0))
);


--
-- Name: sip_profile_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE sip_profile_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sip_profile_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE sip_profile_id_seq OWNED BY sip_profile.id;


--
-- Name: sip_trunks; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE sip_trunks (
    id bigint NOT NULL,
    name character varying(255),
    host character varying(255),
    type character varying(255),
    trunk_no bigint,
    password character varying(255),
    user_name character varying(255),
    proxy character varying(255)
);


--
-- Name: sofia_gateway; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE sofia_gateway (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    channels integer NOT NULL,
    enabled boolean NOT NULL,
    prefix character varying(15) NOT NULL,
    suffix character varying(15) NOT NULL,
    codec character varying(30) NOT NULL,
    username character varying(35) NOT NULL,
    password character varying(35) NOT NULL,
    register boolean NOT NULL,
    proxy character varying(48) NOT NULL,
    extension character varying(50) NOT NULL,
    realm character varying(50) NOT NULL,
    from_domain character varying(50) NOT NULL,
    expire_seconds integer,
    retry_seconds integer,
    caller_id_in_from boolean NOT NULL,
    sip_cid_type character varying(10) NOT NULL,
    date_added timestamp with time zone NOT NULL,
    date_modified timestamp with time zone NOT NULL,
    sip_profile_id integer NOT NULL,
    CONSTRAINT sofia_gateway_channels_check CHECK ((channels >= 0)),
    CONSTRAINT sofia_gateway_expire_seconds_check CHECK ((expire_seconds >= 0)),
    CONSTRAINT sofia_gateway_retry_seconds_check CHECK ((retry_seconds >= 0))
);


--
-- Name: sofia_gateway_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE sofia_gateway_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sofia_gateway_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE sofia_gateway_id_seq OWNED BY sofia_gateway.id;


--
-- Name: user_crm; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE user_crm (
    user_id bigint,
    crm_id bigint
);


--
-- Name: voip_switch; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE voip_switch (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    ip character varying(100) NOT NULL,
    esl_listen_ip character varying(100) NOT NULL,
    esl_listen_port integer NOT NULL,
    esl_password character varying(30) NOT NULL,
    date_added timestamp with time zone NOT NULL,
    date_modified timestamp with time zone NOT NULL,
    CONSTRAINT voip_switch_esl_listen_port_check CHECK ((esl_listen_port >= 0))
);


--
-- Name: voip_switch_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE voip_switch_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: voip_switch_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE voip_switch_id_seq OWNED BY voip_switch.id;


--
-- Name: audio_file id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY audio_file ALTER COLUMN id SET DEFAULT nextval('audio_file_id_seq'::regclass);


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- Name: auth_user id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);


--
-- Name: auth_user_groups id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);


--
-- Name: auth_user_user_permissions id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);


--
-- Name: cdr id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY cdr ALTER COLUMN id SET DEFAULT nextval('cdr_id_seq'::regclass);


--
-- Name: date_dimension id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY date_dimension ALTER COLUMN id SET DEFAULT nextval('date_dimension_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY django_migrations ALTER COLUMN id SET DEFAULT nextval('django_migrations_id_seq'::regclass);


--
-- Name: hangup_cause id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY hangup_cause ALTER COLUMN id SET DEFAULT nextval('hangup_cause_id_seq'::regclass);


--
-- Name: sip_profile id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY sip_profile ALTER COLUMN id SET DEFAULT nextval('sip_profile_id_seq'::regclass);


--
-- Name: sofia_gateway id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY sofia_gateway ALTER COLUMN id SET DEFAULT nextval('sofia_gateway_id_seq'::regclass);


--
-- Name: voip_switch id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY voip_switch ALTER COLUMN id SET DEFAULT nextval('voip_switch_id_seq'::regclass);


--
-- Data for Name: Strategy; Type: TABLE DATA; Schema: public; Owner: -
--

COPY "Strategy" ("Strategy") FROM stdin;
ring-all
longest-idle-agent
round-robin
\.


--
-- Data for Name: agent_details; Type: TABLE DATA; Schema: public; Owner: -
--

COPY agent_details (agent, tier_level, tier_posotion, queue_name) FROM stdin;
\.


--
-- Data for Name: audio_file; Type: TABLE DATA; Schema: public; Owner: -
--

COPY audio_file (id, name, audio_file) FROM stdin;
\.


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: -
--

COPY auth_group (id, name) FROM stdin;
1	Super Admin
2	Admin
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: -
--

COPY auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: -
--

COPY auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can add group	2	add_group
5	Can change group	2	change_group
6	Can delete group	2	delete_group
7	Can add permission	3	add_permission
8	Can change permission	3	change_permission
9	Can delete permission	3	delete_permission
10	Can add user	4	add_user
11	Can change user	4	change_user
12	Can delete user	4	delete_user
13	Can add content type	5	add_contenttype
14	Can change content type	5	change_contenttype
15	Can delete content type	5	delete_contenttype
16	Can add session	6	add_session
17	Can change session	6	change_session
18	Can delete session	6	delete_session
19	Can add users	7	add_users
20	Can change users	7	change_users
21	Can delete users	7	delete_users
22	Can add backup table	8	add_backuptable
23	Can change backup table	8	change_backuptable
24	Can delete backup table	8	delete_backuptable
25	Can add blaclisted tables	9	add_blaclistedtables
26	Can change blaclisted tables	9	change_blaclistedtables
27	Can delete blaclisted tables	9	delete_blaclistedtables
28	Can add call logs	10	add_calllogs
29	Can change call logs	10	change_calllogs
30	Can delete call logs	10	delete_calllogs
31	Can add call parking	11	add_callparking
32	Can change call parking	11	change_callparking
33	Can delete call parking	11	delete_callparking
34	Can add strategy	12	add_strategy
35	Can change strategy	12	change_strategy
36	Can delete strategy	12	delete_strategy
37	Can add agent details	13	add_agentdetails
38	Can change agent details	13	change_agentdetails
39	Can delete agent details	13	delete_agentdetails
40	Can add auth user	14	add_authuser
41	Can change auth user	14	change_authuser
42	Can delete auth user	14	delete_authuser
43	Can add call queues	15	add_callqueues
44	Can change call queues	15	change_callqueues
45	Can delete call queues	15	delete_callqueues
46	Can add call queue users	16	add_callqueueusers
47	Can change call queue users	16	change_callqueueusers
48	Can delete call queue users	16	delete_callqueueusers
49	Can add chat log	17	add_chatlog
50	Can change chat log	17	change_chatlog
51	Can delete chat log	17	delete_chatlog
52	Can add contacts	18	add_contacts
53	Can change contacts	18	change_contacts
54	Can delete contacts	18	delete_contacts
55	Can add phone vendors	19	add_phonevendors
56	Can change phone vendors	19	change_phonevendors
57	Can delete phone vendors	19	delete_phonevendors
58	Can add devices	20	add_devices
59	Can change devices	20	change_devices
60	Can delete devices	20	delete_devices
61	Can add dialplan conditions	21	add_dialplanconditions
62	Can change dialplan conditions	21	change_dialplanconditions
63	Can delete dialplan conditions	21	delete_dialplanconditions
64	Can add dialplan actions	22	add_dialplanactions
65	Can change dialplan actions	22	change_dialplanactions
66	Can delete dialplan actions	22	delete_dialplanactions
67	Can add dialplan	23	add_dialplan
68	Can change dialplan	23	change_dialplan
69	Can delete dialplan	23	delete_dialplan
70	Can add digital receptionist	24	add_digitalreceptionist
71	Can change digital receptionist	24	change_digitalreceptionist
72	Can delete digital receptionist	24	delete_digitalreceptionist
73	Can add digital actions	25	add_digitalactions
74	Can change digital actions	25	change_digitalactions
75	Can delete digital actions	25	delete_digitalactions
76	Can add domains	26	add_domains
77	Can change domains	26	change_domains
78	Can delete domains	26	delete_domains
79	Can add email setup	27	add_emailsetup
80	Can change email setup	27	change_emailsetup
81	Can delete email setup	27	delete_emailsetup
82	Can add extension	28	add_extension
83	Can change extension	28	change_extension
84	Can delete extension	28	delete_extension
85	Can add hangupcause	29	add_hangupcause
86	Can change hangupcause	29	change_hangupcause
87	Can delete hangupcause	29	delete_hangupcause
88	Can add Sofia gateway	30	add_sofiagateway
89	Can change Sofia gateway	30	change_sofiagateway
90	Can delete Sofia gateway	30	delete_sofiagateway
91	Can add VoIP Switch	31	add_voipswitch
92	Can change VoIP Switch	31	change_voipswitch
93	Can delete VoIP Switch	31	delete_voipswitch
94	Can add SIP profile	32	add_sipprofile
95	Can change SIP profile	32	change_sipprofile
96	Can delete SIP profile	32	delete_sipprofile
97	Can add CDR	33	add_cdr
98	Can change CDR	33	change_cdr
99	Can delete CDR	33	delete_cdr
100	Can add date dimension	34	add_dimdate
101	Can change date dimension	34	change_dimdate
102	Can delete date dimension	34	delete_dimdate
103	Can add ext groups	35	add_extgroups
104	Can change ext groups	35	change_extgroups
105	Can delete ext groups	35	delete_extgroups
106	Can add inboundrules	36	add_inboundrules
107	Can change inboundrules	36	change_inboundrules
108	Can delete inboundrules	36	delete_inboundrules
109	Can add document	37	add_document
110	Can change document	37	change_document
111	Can delete document	37	delete_document
112	Can add outbound rules	38	add_outboundrules
113	Can change outbound rules	38	change_outboundrules
114	Can delete outbound rules	38	delete_outboundrules
115	Can add recordings	39	add_recordings
116	Can change recordings	39	change_recordings
117	Can delete recordings	39	delete_recordings
118	Can add ringgroup strategy	40	add_ringgroupstrategy
119	Can change ringgroup strategy	40	change_ringgroupstrategy
120	Can delete ringgroup strategy	40	delete_ringgroupstrategy
121	Can add ring groups	41	add_ringgroups
122	Can change ring groups	41	change_ringgroups
123	Can delete ring groups	41	delete_ringgroups
124	Can add ringgroup user	42	add_ringgroupuser
125	Can change ringgroup user	42	change_ringgroupuser
126	Can delete ringgroup user	42	delete_ringgroupuser
127	Can add sip trunks	43	add_siptrunks
128	Can change sip trunks	43	change_siptrunks
129	Can delete sip trunks	43	delete_siptrunks
130	Can add auth user groups	44	add_authusergroups
131	Can change auth user groups	44	change_authusergroups
132	Can delete auth user groups	44	delete_authusergroups
133	Can add agent details	45	add_agentdetails
134	Can change agent details	45	change_agentdetails
135	Can delete agent details	45	delete_agentdetails
136	Can add auth user user permissions	46	add_authuseruserpermissions
137	Can change auth user user permissions	46	change_authuseruserpermissions
138	Can delete auth user user permissions	46	delete_authuseruserpermissions
139	Can add django migrations	47	add_djangomigrations
140	Can change django migrations	47	change_djangomigrations
141	Can delete django migrations	47	delete_djangomigrations
142	Can add auth user	48	add_authuser
143	Can change auth user	48	change_authuser
144	Can delete auth user	48	delete_authuser
145	Can add user roles	49	add_userroles
146	Can change user roles	49	change_userroles
147	Can delete user roles	49	delete_userroles
148	Can add django session	50	add_djangosession
149	Can change django session	50	change_djangosession
150	Can delete django session	50	delete_djangosession
151	Can add users	51	add_users
152	Can change users	51	change_users
153	Can delete users	51	delete_users
154	Can add auth permission	52	add_authpermission
155	Can change auth permission	52	change_authpermission
156	Can delete auth permission	52	delete_authpermission
157	Can add django admin log	53	add_djangoadminlog
158	Can change django admin log	53	change_djangoadminlog
159	Can delete django admin log	53	delete_djangoadminlog
160	Can add django content type	54	add_djangocontenttype
161	Can change django content type	54	change_djangocontenttype
162	Can delete django content type	54	delete_djangocontenttype
163	Can add call queues	55	add_callqueues
164	Can change call queues	55	change_callqueues
165	Can delete call queues	55	delete_callqueues
166	Can add domains	56	add_domains
167	Can change domains	56	change_domains
168	Can delete domains	56	delete_domains
169	Can add auth group permissions	57	add_authgrouppermissions
170	Can change auth group permissions	57	change_authgrouppermissions
171	Can delete auth group permissions	57	delete_authgrouppermissions
172	Can add auth group	58	add_authgroup
173	Can change auth group	58	change_authgroup
174	Can delete auth group	58	delete_authgroup
175	Can add roles	59	add_roles
176	Can change roles	59	change_roles
177	Can delete roles	59	delete_roles
178	Can add crm	60	add_crm
179	Can change crm	60	change_crm
180	Can delete crm	60	delete_crm
181	Can add user crm	61	add_usercrm
182	Can change user crm	61	change_usercrm
183	Can delete user crm	61	delete_usercrm
\.


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: -
--

COPY auth_user (id, password, last_login, is_superuser, first_name, last_name, email, is_staff, is_active, date_joined, is_user, is_admin, is_agent, is_enabled, user_group, domain, outbound_caller_id, mobile, call_queue, username) FROM stdin;
4	pbkdf2_sha256$36000$aIvALzxsNZJd$SDPJAh4DK8Fm+ySo/+xzewSDv9ntEi8mKAbT0H3kGs8=	\N	f	Mona	Patil	sohanpatil@gmail.com	f	t	2017-11-24 08:33:24.608+00	\N	\N	\N	t	1	Internal	11	22	\N	1003
5	pbkdf2_sha256$36000$e6pTtyi5eaUt$apwsFe5FH0mOrGVk3aRjI6nDE34Y4kjEyY+uAbhAEpo=	\N	f	Dan	Jones	djones@usethegeeks.co.uk	f	t	2017-11-30 13:58:44.779+00	\N	\N	\N	t	1	Internal	132	123	\N	1007
6	pbkdf2_sha256$36000$eFrZ7J2opa6W$1eQWd1v73IF2vvhY3CDaS2/zF1z6iOmdkPB0AalAgkw=	\N	f	Ankit	Kumar	akumar@usethegeeks.com	f	t	2017-11-30 14:01:31.235+00	\N	\N	\N	t	1	Internal	9650244466	9650244466	\N	1008
7	pbkdf2_sha256$36000$2GlTz1lNsfFR$or2/XrCerzcB3YJf9bJrCLDZhPPpPmygu19J1HFtEP4=	\N	f	Ankit1	Kumar	akumar@usethegeeks.com	f	t	2017-11-30 14:12:22.425+00	\N	\N	\N	t	1	Internal	965044466	9650244466	\N	1010
1	pbkdf2_sha256$36000$XDw4FuxQ3MKe$FYnS+93pnJY05ML+Ia9v6O+NsGkeYwYy15jvG6ShoAI=	2018-02-01 08:26:16.054+00	t			daya6252@gmail.com	t	t	2017-11-22 15:14:08.855+00	\N	\N	\N	\N	\N	\N	\N	\N	\N	1000
13	pbkdf2_sha256$36000$YCIzhPGRn1OB$+HcoWlK4HtAn8bg/jByhN5lLusyIT8cDT/aRQSKzBsg=	\N	f	Vijay	Umalti	umalti@gmail.com	f	t	2018-02-01 08:53:52.063+00	\N	\N	\N	t	1	Internal	1009	9739739739	\N	1009
14	pbkdf2_sha256$36000$8Vpyuw4MoZKe$rut1U1egFJCQKBV6SxU7sfjNXZv6PP1bhudJL10V068=	\N	f	Vijay	Umaluti	umaluti1@gmail.com	f	t	2018-02-01 08:56:14.471+00	\N	\N	\N	t	1	Internal	1004	9988776655	\N	1004
10	pbkdf2_sha256$36000$ZZEWVmlP7qlU$FQYJqf36CS2VD+FXhIcnn1V67s1qynvK9DIhCvrsZuw=	\N	f	Dayanand	Pattar	daya6252@gmail.com	f	t	2017-12-06 15:51:27.363+00	\N	\N	\N	t	1	Internal	8904281258	8904281258	\N	1019
11	pbkdf2_sha256$36000$5MW9DVrcjp58$1kZOChHeZZSKazBXhMt1rSd1cLuVKR4TbTuepo+ohho=	\N	f	Mony	P	sp@gmail.com	f	t	2017-12-07 06:15:28.818+00	\N	\N	\N	t	1	Internal	11	2	\N	1005
12	pbkdf2_sha256$36000$RGB5kmWHHOBq$dYQt/8jolBNN1sTgiXttAeXl261xiTNGQ7/XStajNNs=	\N	f	Jhon	C	abc@gmail.com	f	t	2017-12-07 06:38:17.406+00	\N	\N	\N	t	2	Internal	123	789	\N	1006
2	pbkdf2_sha256$36000$91hZkQKsHnEK$w4X339aLH59C6SxkY2AuXH/31WQByYS114+hfU07c5s=	2017-12-08 10:20:27.839+00	f	sohan	p	sohanpatil.99@gmail.com	f	t	2017-11-23 08:25:58.583+00	\N	\N	\N	t	1	Internal	34	44	\N	1001
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: -
--

COPY auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: -
--

COPY auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Data for Name: blaclisted_tables; Type: TABLE DATA; Schema: public; Owner: -
--

COPY blaclisted_tables (id, "user", description) FROM stdin;
\.


--
-- Data for Name: call_logs; Type: TABLE DATA; Schema: public; Owner: -
--

COPY call_logs (id, date_time, "from", "to") FROM stdin;
\.


--
-- Data for Name: call_queue_users; Type: TABLE DATA; Schema: public; Owner: -
--

COPY call_queue_users (call_queue, "user", id) FROM stdin;
1	1001	1
1	1001	2
1	1003	3
1	1007	0
\.


--
-- Data for Name: call_queues; Type: TABLE DATA; Schema: public; Owner: -
--

COPY call_queues (id, extension, poll_strategy, timeout, queue_name) FROM stdin;
1	112233	ring-all	20	CallQ_test
\.


--
-- Data for Name: cdr; Type: TABLE DATA; Schema: public; Owner: -
--

COPY cdr (id, customer, customer_ip, uuid, bleg_uuid, caller_id_number, destination_number, chan_name, start_stamp, answered_stamp, end_stamp, duration, effectiv_duration, effective_duration, billsec, read_codec, write_codec, hangup_cause, hangup_cause_q850, cost_rate, total_sell, total_cost, prefix, rate, init_block, block_min_duration, sip_user_agent, sip_rtp_rxstat, sip_rtp_txstat, switchname, switch_ipv4, hangup_disposition, sip_hangup_cause, sell_destination, cost_destination, gateway_id) FROM stdin;
\.


--
-- Data for Name: chat_log; Type: TABLE DATA; Schema: public; Owner: -
--

COPY chat_log (id, "from", "to", message) FROM stdin;
\.


--
-- Data for Name: contacts; Type: TABLE DATA; Schema: public; Owner: -
--

COPY contacts (id, first_name, last_name, company, business, email, mobile) FROM stdin;
2	vinayak	sanga	mylife spectrum solutions pvt ltd	mylife spectrum solutions pvt ltd	vinayak@pristinesofts.com	9197394198
\.


--
-- Data for Name: crm; Type: TABLE DATA; Schema: public; Owner: -
--

COPY crm (id, crm_name, contact_match_link, open_contact_link, create_contact_link, journeling_link) FROM stdin;
\.


--
-- Data for Name: date_dimension; Type: TABLE DATA; Schema: public; Owner: -
--

COPY date_dimension (id, date, day, day_of_week, hour, month, quarter, year) FROM stdin;
\.


--
-- Data for Name: devices; Type: TABLE DATA; Schema: public; Owner: -
--

COPY devices (id, extension, device_model, name, user_id, password, ip_address, mac_address) FROM stdin;
\.


--
-- Data for Name: dialplan; Type: TABLE DATA; Schema: public; Owner: -
--

COPY dialplan (id, name, condition1, condition2, action1, action2, "order", enabled, description, value1, value2, action_value1, action_value2) FROM stdin;
2	1003	caller_id_name	destination_number	bridge	\N	\N	\N		sofia/gateway/flowrouteold/$1		1000	\N
\.


--
-- Data for Name: dialplan_actions; Type: TABLE DATA; Schema: public; Owner: -
--

COPY dialplan_actions (id, action) FROM stdin;
1	answer
2	bridge
3	log
4	hangup
5	playback
6	set
7	transfer
\.


--
-- Data for Name: dialplan_conditions; Type: TABLE DATA; Schema: public; Owner: -
--

COPY dialplan_conditions (id, condition) FROM stdin;
1	caller_id_name
2	destination_number
3	direction
4	channel_name
5	call_timeout
6	state
7	bridge_hangup_cause
\.


--
-- Data for Name: digital_actions; Type: TABLE DATA; Schema: public; Owner: -
--

COPY digital_actions (action) FROM stdin;
transfer
hang up
\.


--
-- Data for Name: digital_receptionist; Type: TABLE DATA; Schema: public; Owner: -
--

COPY digital_receptionist (name, prompt, extension, action_0, action_1, action_2, action_3, action_4, action_5, action_6, action_7, action_8, action_9, ac_value_0, ac_value_1, ac_value_2, ac_value_3, ac_value_4, ac_value_5, ac_value_6, ac_value_7, ac_value_8, ac_value_9, inactive_second, inactive_action, invalid_action, id) FROM stdin;
testing	testing	1001	transfer	transfer	transfer	transfer	transfer	transfer	transfer	transfer	transfer	transfer	\N										20	hang up	hang up	2
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: -
--

COPY django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: -
--

COPY django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	group
3	auth	permission
4	auth	user
5	contenttypes	contenttype
6	sessions	session
7	loginpage	users
8	backuprestore	backuptable
9	blocklist	blaclistedtables
10	calllogs	calllogs
11	callparking	callparking
12	callqueues	strategy
13	callqueues	agentdetails
14	callqueues	authuser
15	callqueues	callqueues
16	callqueues	callqueueusers
17	chatlogs	chatlog
18	contacts	contacts
19	devices	phonevendors
20	devices	devices
21	dialplans	dialplanconditions
22	dialplans	dialplanactions
23	dialplans	dialplan
24	digitalreception	digitalreceptionist
25	digitalreception	digitalactions
26	domains	domains
27	emailsetup	emailsetup
28	extensions	extension
29	eslswitch	hangupcause
30	eslswitch	sofiagateway
31	eslswitch	voipswitch
32	eslswitch	sipprofile
33	eslswitch	cdr
34	eslswitch	dimdate
35	groups	extgroups
36	inboundrules	inboundrules
37	musiconhold	document
38	outboundrules	outboundrules
39	recordings	recordings
40	ringgroups	ringgroupstrategy
41	ringgroups	ringgroups
42	ringgroups	ringgroupuser
43	siptrunks	siptrunks
44	utgusers	authusergroups
45	utgusers	agentdetails
46	utgusers	authuseruserpermissions
47	utgusers	djangomigrations
48	utgusers	authuser
49	utgusers	userroles
50	utgusers	djangosession
51	utgusers	users
52	utgusers	authpermission
53	utgusers	djangoadminlog
54	utgusers	djangocontenttype
55	utgusers	callqueues
56	utgusers	domains
57	utgusers	authgrouppermissions
58	utgusers	authgroup
59	utgusers	roles
60	crmint	crm
61	crmint	usercrm
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: -
--

COPY django_migrations (id, app, name, applied) FROM stdin;
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: -
--

COPY django_session (session_key, session_data, expire_date) FROM stdin;
nuks6l6r19rnp1s183hmsnnpl88dub40	NDgxY2M2NjNjOTFlZjIwZjM5MmE4ZjZhNjhjZGRlNzBkYjNmYWRlNDp7Il9hdXRoX3VzZXJfaGFzaCI6IjFlMGJjZGJhMmMzNTM0ZWM3ZjVjYzQ5NzlkYzAxZDQ1YTIwNTA0OWIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2017-12-06 15:22:32.481+00
iw265u5cw8cm2lu7h2vizuj0ol3rkams	NDgxY2M2NjNjOTFlZjIwZjM5MmE4ZjZhNjhjZGRlNzBkYjNmYWRlNDp7Il9hdXRoX3VzZXJfaGFzaCI6IjFlMGJjZGJhMmMzNTM0ZWM3ZjVjYzQ5NzlkYzAxZDQ1YTIwNTA0OWIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2017-12-08 05:47:45.82+00
x5b8hdlmel86uy4pxbjvjpdaztepg02p	NDgxY2M2NjNjOTFlZjIwZjM5MmE4ZjZhNjhjZGRlNzBkYjNmYWRlNDp7Il9hdXRoX3VzZXJfaGFzaCI6IjFlMGJjZGJhMmMzNTM0ZWM3ZjVjYzQ5NzlkYzAxZDQ1YTIwNTA0OWIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2017-12-09 06:40:34.013+00
wxrfgqtap02kdc5d401iiruqnasg8zko	NDgxY2M2NjNjOTFlZjIwZjM5MmE4ZjZhNjhjZGRlNzBkYjNmYWRlNDp7Il9hdXRoX3VzZXJfaGFzaCI6IjFlMGJjZGJhMmMzNTM0ZWM3ZjVjYzQ5NzlkYzAxZDQ1YTIwNTA0OWIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2017-12-09 14:12:56.682+00
k6w0nvv5j1m6w37pb2hgk1zg4oqq1tkh	MDk4MWJlZDc4MzJhNjZkMTcwNTNkZDA0ZDU3ODc0MDc3YjdiMGU4ODp7Il9hdXRoX3VzZXJfaGFzaCI6IjFlMGJjZGJhMmMzNTM0ZWM3ZjVjYzQ5NzlkYzAxZDQ1YTIwNTA0OWIiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2017-12-20 14:39:52.663+00
di42wn01fo4kwozq8qbrdwxarw2t16sy	NDgxY2M2NjNjOTFlZjIwZjM5MmE4ZjZhNjhjZGRlNzBkYjNmYWRlNDp7Il9hdXRoX3VzZXJfaGFzaCI6IjFlMGJjZGJhMmMzNTM0ZWM3ZjVjYzQ5NzlkYzAxZDQ1YTIwNTA0OWIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2017-12-20 15:56:18.747+00
51kv6fiepf6lwatgexvx0w34gxhvpycy	NmRkYmVlYjUxODg0M2U0Y2FlMDM4ZDRhOTQ3ZWZiYjBlYjE0ZWQ2Yzp7Il9hdXRoX3VzZXJfaGFzaCI6ImZhMWEzNTQ1MTczYzdlZDUwZWM5NjA2YTA4MmZhOGY3ZDAwZjEwZjIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=	2017-12-21 06:10:58.843+00
6p56oimx2d0r4bvqtvdd1j4uq8dno14a	NmRkYmVlYjUxODg0M2U0Y2FlMDM4ZDRhOTQ3ZWZiYjBlYjE0ZWQ2Yzp7Il9hdXRoX3VzZXJfaGFzaCI6ImZhMWEzNTQ1MTczYzdlZDUwZWM5NjA2YTA4MmZhOGY3ZDAwZjEwZjIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=	2017-12-21 06:52:16.406+00
74tolx23fq528emhlijfke4qh4sbdjg3	NmRkYmVlYjUxODg0M2U0Y2FlMDM4ZDRhOTQ3ZWZiYjBlYjE0ZWQ2Yzp7Il9hdXRoX3VzZXJfaGFzaCI6ImZhMWEzNTQ1MTczYzdlZDUwZWM5NjA2YTA4MmZhOGY3ZDAwZjEwZjIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=	2017-12-22 06:33:36.212+00
aztodqp457m7frksksnckac5f9ivblo7	NmRkYmVlYjUxODg0M2U0Y2FlMDM4ZDRhOTQ3ZWZiYjBlYjE0ZWQ2Yzp7Il9hdXRoX3VzZXJfaGFzaCI6ImZhMWEzNTQ1MTczYzdlZDUwZWM5NjA2YTA4MmZhOGY3ZDAwZjEwZjIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=	2017-12-22 10:20:27.849+00
rquxbaunfih2g38ddtf3q778s1ql6kow	NDgxY2M2NjNjOTFlZjIwZjM5MmE4ZjZhNjhjZGRlNzBkYjNmYWRlNDp7Il9hdXRoX3VzZXJfaGFzaCI6IjFlMGJjZGJhMmMzNTM0ZWM3ZjVjYzQ5NzlkYzAxZDQ1YTIwNTA0OWIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2017-12-22 11:15:48.662+00
lp31j9alnqu46k5rzxhirxbk3op9qxix	MDk4MWJlZDc4MzJhNjZkMTcwNTNkZDA0ZDU3ODc0MDc3YjdiMGU4ODp7Il9hdXRoX3VzZXJfaGFzaCI6IjFlMGJjZGJhMmMzNTM0ZWM3ZjVjYzQ5NzlkYzAxZDQ1YTIwNTA0OWIiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2017-12-22 11:21:11.241+00
vwmjlrpbel25ok30uebs6ihadcufw4ee	MDk4MWJlZDc4MzJhNjZkMTcwNTNkZDA0ZDU3ODc0MDc3YjdiMGU4ODp7Il9hdXRoX3VzZXJfaGFzaCI6IjFlMGJjZGJhMmMzNTM0ZWM3ZjVjYzQ5NzlkYzAxZDQ1YTIwNTA0OWIiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2018-01-12 11:52:18.222+00
2nlexajs1f9f0po8ranx42tmyut9pdb6	MDk4MWJlZDc4MzJhNjZkMTcwNTNkZDA0ZDU3ODc0MDc3YjdiMGU4ODp7Il9hdXRoX3VzZXJfaGFzaCI6IjFlMGJjZGJhMmMzNTM0ZWM3ZjVjYzQ5NzlkYzAxZDQ1YTIwNTA0OWIiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2018-01-12 12:33:02.492+00
qixv66y48mhuupzmvz5gttlvyaofbaej	NDgxY2M2NjNjOTFlZjIwZjM5MmE4ZjZhNjhjZGRlNzBkYjNmYWRlNDp7Il9hdXRoX3VzZXJfaGFzaCI6IjFlMGJjZGJhMmMzNTM0ZWM3ZjVjYzQ5NzlkYzAxZDQ1YTIwNTA0OWIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2018-01-13 05:46:30.132+00
h36xjeu95mrldvgfox81h4hjmpxf7g0o	NDgxY2M2NjNjOTFlZjIwZjM5MmE4ZjZhNjhjZGRlNzBkYjNmYWRlNDp7Il9hdXRoX3VzZXJfaGFzaCI6IjFlMGJjZGJhMmMzNTM0ZWM3ZjVjYzQ5NzlkYzAxZDQ1YTIwNTA0OWIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2018-01-18 06:38:06.405+00
cygtaqo89hh2telsve4dsn2y05lqqfx6	NDgxY2M2NjNjOTFlZjIwZjM5MmE4ZjZhNjhjZGRlNzBkYjNmYWRlNDp7Il9hdXRoX3VzZXJfaGFzaCI6IjFlMGJjZGJhMmMzNTM0ZWM3ZjVjYzQ5NzlkYzAxZDQ1YTIwNTA0OWIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2018-01-18 07:16:19.353+00
gu4fl8z9o4bo7kj2r49riqrpz2r5l964	NDgxY2M2NjNjOTFlZjIwZjM5MmE4ZjZhNjhjZGRlNzBkYjNmYWRlNDp7Il9hdXRoX3VzZXJfaGFzaCI6IjFlMGJjZGJhMmMzNTM0ZWM3ZjVjYzQ5NzlkYzAxZDQ1YTIwNTA0OWIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2018-02-15 07:47:18.314+00
xtnpv49z23ungah8t7oulp1jc9oxbug4	NDgxY2M2NjNjOTFlZjIwZjM5MmE4ZjZhNjhjZGRlNzBkYjNmYWRlNDp7Il9hdXRoX3VzZXJfaGFzaCI6IjFlMGJjZGJhMmMzNTM0ZWM3ZjVjYzQ5NzlkYzAxZDQ1YTIwNTA0OWIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2018-02-15 08:26:16.057+00
\.


--
-- Data for Name: domains; Type: TABLE DATA; Schema: public; Owner: -
--

COPY domains (id, domain_name, is_enabled, description, max_connections) FROM stdin;
1	Internal	t	testing	10
\.


--
-- Data for Name: email_setup; Type: TABLE DATA; Schema: public; Owner: -
--

COPY email_setup (smtp_host, port, user_name, password, template_name, from_field, subject, body) FROM stdin;
smtp.sendgrid.net	25	apickett@usethegeeks.co.uk	Utgesx009	register	apickett@usethegeeks.co.uk	welcome utg	Hi user welcome to utg
\.


--
-- Data for Name: hangup_cause; Type: TABLE DATA; Schema: public; Owner: -
--

COPY hangup_cause (id, code, enumeration, cause, description, date_added, date_modified) FROM stdin;
\.


--
-- Data for Name: inboundrules; Type: TABLE DATA; Schema: public; Owner: -
--

COPY inboundrules (id, type, name, trunk, did_number, in_office_routing, out_office_routing) FROM stdin;
1	Normal	Call_inbound	956122	1003	1000	1000
\.


--
-- Data for Name: outbound_rules; Type: TABLE DATA; Schema: public; Owner: -
--

COPY outbound_rules (id, name, call_from, prefix, ext_group, route1, route2, route3, route4, route5) FROM stdin;
1	Sohan	1001	9561	1000	\N	\N	\N	\N	\N
\.


--
-- Data for Name: phone_vendors; Type: TABLE DATA; Schema: public; Owner: -
--

COPY phone_vendors (id, vendor, model, version) FROM stdin;
\.


--
-- Data for Name: ring_groups; Type: TABLE DATA; Schema: public; Owner: -
--

COPY ring_groups (id, extension, name, type, if_no_ans) FROM stdin;
2	313131	Hunt	Hunt Group	transfer
\.


--
-- Data for Name: ringgroup_strategy; Type: TABLE DATA; Schema: public; Owner: -
--

COPY ringgroup_strategy (name) FROM stdin;
Ring All
Hunt Group
\.


--
-- Data for Name: ringgroup_user; Type: TABLE DATA; Schema: public; Owner: -
--

COPY ringgroup_user (ringgroup_id, user_id, id) FROM stdin;
2	1002	1
2	1002	2
2	1001	3
2	1002	4
2	1001	5
2	1002	6
2	1001	7
2	1002	8
2	1002	9
2	1002	10
2	1002	11
2	1001	12
2	1002	13
2	1001	14
2	1002	15
2	1001	16
2	1002	17
2	1001	18
2	1001	19
2	1003	20
2	1001	21
2	1003	22
2	1001	23
2	1002	24
2	1001	25
2	1003	26
2	1001	27
2	1003	28
2	1001	29
2	1003	30
2	1001	31
2	1003	32
2	1001	33
2	1003	34
2	1003	35
2	1005	36
2	1006	37
2	1005	38
2	1006	39
\.


--
-- Data for Name: sip_profile; Type: TABLE DATA; Schema: public; Owner: -
--

COPY sip_profile (id, name, user_agent, ext_rtp_ip, ext_sip_ip, rtp_ip, sip_ip, sip_port, disable_transcoding, accept_blind_reg, disable_register, apply_inbound_acl, auth_calls, log_auth_failures, inbound_codec_prefs, outbound_codec_prefs, aggressive_nat_detection, "NDLB_rec_in_nat_reg_c", "NDLB_force_rport", "NDLB_broken_auth_hash", enable_timer, session_timeout, rtp_rewrite_timestamps, pass_rfc2833, date_added, date_modified) FROM stdin;
\.


--
-- Data for Name: sip_trunks; Type: TABLE DATA; Schema: public; Owner: -
--

COPY sip_trunks (id, name, host, type, trunk_no, password, user_name, proxy) FROM stdin;
1	9561227548	34.215.16.22	\N	\N	1234	\N	sip.flowroute.com
\.


--
-- Data for Name: sofia_gateway; Type: TABLE DATA; Schema: public; Owner: -
--

COPY sofia_gateway (id, name, channels, enabled, prefix, suffix, codec, username, password, register, proxy, extension, realm, from_domain, expire_seconds, retry_seconds, caller_id_in_from, sip_cid_type, date_added, date_modified, sip_profile_id) FROM stdin;
\.


--
-- Data for Name: user_crm; Type: TABLE DATA; Schema: public; Owner: -
--

COPY user_crm (user_id, crm_id) FROM stdin;
\.


--
-- Data for Name: voip_switch; Type: TABLE DATA; Schema: public; Owner: -
--

COPY voip_switch (id, name, ip, esl_listen_ip, esl_listen_port, esl_password, date_added, date_modified) FROM stdin;
\.


--
-- Name: audio_file_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('audio_file_id_seq', 1, false);


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('auth_group_id_seq', 1, false);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 1, false);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('auth_permission_id_seq', 183, true);


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('auth_user_id_seq', 14, true);


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('auth_user_user_permissions_id_seq', 1, false);


--
-- Name: call_queue_userseq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('call_queue_userseq', 3, true);


--
-- Name: cdr_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('cdr_id_seq', 1, false);


--
-- Name: date_dimension_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('date_dimension_id_seq', 1, false);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 1, false);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('django_content_type_id_seq', 61, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('django_migrations_id_seq', 47, true);


--
-- Name: hangup_cause_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('hangup_cause_id_seq', 1, false);


--
-- Name: ringgroup_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('ringgroup_seq', 39, true);


--
-- Name: sip_profile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('sip_profile_id_seq', 1, false);


--
-- Name: sofia_gateway_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('sofia_gateway_id_seq', 1, false);


--
-- Name: voip_switch_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('voip_switch_id_seq', 1, false);


--
-- Name: Strategy Strategy_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY "Strategy"
    ADD CONSTRAINT "Strategy_pkey" PRIMARY KEY ("Strategy");


--
-- Name: audio_file audio_file_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY audio_file
    ADD CONSTRAINT audio_file_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: call_logs call_logs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY call_logs
    ADD CONSTRAINT call_logs_pkey PRIMARY KEY (id);


--
-- Name: call_queue_users call_queue_users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY call_queue_users
    ADD CONSTRAINT call_queue_users_pkey PRIMARY KEY (id);


--
-- Name: call_queues call_queues_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY call_queues
    ADD CONSTRAINT call_queues_pkey PRIMARY KEY (id);


--
-- Name: cdr cdr_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cdr
    ADD CONSTRAINT cdr_pkey PRIMARY KEY (id);


--
-- Name: chat_log chat_log_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY chat_log
    ADD CONSTRAINT chat_log_pkey PRIMARY KEY (id);


--
-- Name: contacts contacts_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY contacts
    ADD CONSTRAINT contacts_pkey PRIMARY KEY (id);


--
-- Name: crm crm_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY crm
    ADD CONSTRAINT crm_pkey PRIMARY KEY (id);


--
-- Name: date_dimension date_dimension_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY date_dimension
    ADD CONSTRAINT date_dimension_pkey PRIMARY KEY (id);


--
-- Name: devices devices_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY devices
    ADD CONSTRAINT devices_pkey PRIMARY KEY (id);


--
-- Name: digital_actions digital_actions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY digital_actions
    ADD CONSTRAINT digital_actions_pkey PRIMARY KEY (action);


--
-- Name: digital_receptionist digital_receptionist_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY digital_receptionist
    ADD CONSTRAINT digital_receptionist_pkey PRIMARY KEY (id);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: domains domains_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY domains
    ADD CONSTRAINT domains_pkey PRIMARY KEY (id);


--
-- Name: email_setup email_setup_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY email_setup
    ADD CONSTRAINT email_setup_pkey PRIMARY KEY (smtp_host);


--
-- Name: hangup_cause hangup_cause_code_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY hangup_cause
    ADD CONSTRAINT hangup_cause_code_key UNIQUE (code);


--
-- Name: hangup_cause hangup_cause_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY hangup_cause
    ADD CONSTRAINT hangup_cause_pkey PRIMARY KEY (id);


--
-- Name: inboundrules inboundrules_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY inboundrules
    ADD CONSTRAINT inboundrules_pkey PRIMARY KEY (id);


--
-- Name: outbound_rules outbound_rules_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY outbound_rules
    ADD CONSTRAINT outbound_rules_pkey PRIMARY KEY (id);


--
-- Name: phone_vendors phone_vendors_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY phone_vendors
    ADD CONSTRAINT phone_vendors_pkey PRIMARY KEY (id);


--
-- Name: ring_groups ring_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY ring_groups
    ADD CONSTRAINT ring_groups_pkey PRIMARY KEY (id);


--
-- Name: ringgroup_strategy ringgroup_strategy_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY ringgroup_strategy
    ADD CONSTRAINT ringgroup_strategy_pkey PRIMARY KEY (name);


--
-- Name: ringgroup_user ringgroup_user_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY ringgroup_user
    ADD CONSTRAINT ringgroup_user_pkey PRIMARY KEY (id);


--
-- Name: sip_profile sip_profile_name_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY sip_profile
    ADD CONSTRAINT sip_profile_name_key UNIQUE (name);


--
-- Name: sip_profile sip_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY sip_profile
    ADD CONSTRAINT sip_profile_pkey PRIMARY KEY (id);


--
-- Name: sip_profile sip_profile_sip_ip_sip_port_ef458fd9_uniq; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY sip_profile
    ADD CONSTRAINT sip_profile_sip_ip_sip_port_ef458fd9_uniq UNIQUE (sip_ip, sip_port);


--
-- Name: sip_trunks sip_trunks_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY sip_trunks
    ADD CONSTRAINT sip_trunks_pkey PRIMARY KEY (id);


--
-- Name: sofia_gateway sofia_gateway_name_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY sofia_gateway
    ADD CONSTRAINT sofia_gateway_name_key UNIQUE (name);


--
-- Name: sofia_gateway sofia_gateway_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY sofia_gateway
    ADD CONSTRAINT sofia_gateway_pkey PRIMARY KEY (id);


--
-- Name: voip_switch voip_switch_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY voip_switch
    ADD CONSTRAINT voip_switch_pkey PRIMARY KEY (id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX auth_group_name_a6ea08ec_like ON auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX auth_user_groups_group_id_97559544 ON auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON auth_user_user_permissions USING btree (user_id);


--
-- Name: cdr_cost_destination_e7eaa97b; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX cdr_cost_destination_e7eaa97b ON cdr USING btree (cost_destination);


--
-- Name: cdr_cost_destination_e7eaa97b_like; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX cdr_cost_destination_e7eaa97b_like ON cdr USING btree (cost_destination varchar_pattern_ops);


--
-- Name: cdr_gateway_id_f388be02; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX cdr_gateway_id_f388be02 ON cdr USING btree (gateway_id);


--
-- Name: cdr_hangup_cause_632e2be4; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX cdr_hangup_cause_632e2be4 ON cdr USING btree (hangup_cause);


--
-- Name: cdr_hangup_cause_632e2be4_like; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX cdr_hangup_cause_632e2be4_like ON cdr USING btree (hangup_cause varchar_pattern_ops);


--
-- Name: cdr_sell_destination_ad163a41; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX cdr_sell_destination_ad163a41 ON cdr USING btree (sell_destination);


--
-- Name: cdr_sell_destination_ad163a41_like; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX cdr_sell_destination_ad163a41_like ON cdr USING btree (sell_destination varchar_pattern_ops);


--
-- Name: cdr_start_stamp_24d9ddf7; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX cdr_start_stamp_24d9ddf7 ON cdr USING btree (start_stamp);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX django_session_expire_date_a5c62663 ON django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX django_session_session_key_c0390e0f_like ON django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: sip_profile_name_9b0943d2_like; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX sip_profile_name_9b0943d2_like ON sip_profile USING btree (name varchar_pattern_ops);


--
-- Name: sofia_gateway_name_0ab1e693_like; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX sofia_gateway_name_0ab1e693_like ON sofia_gateway USING btree (name varchar_pattern_ops);


--
-- Name: sofia_gateway_sip_profile_id_9b8a1f4f; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX sofia_gateway_sip_profile_id_9b8a1f4f ON sofia_gateway USING btree (sip_profile_id);


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: cdr cdr_gateway_id_f388be02_fk_sofia_gateway_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY cdr
    ADD CONSTRAINT cdr_gateway_id_f388be02_fk_sofia_gateway_id FOREIGN KEY (gateway_id) REFERENCES sofia_gateway(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: sofia_gateway sofia_gateway_sip_profile_id_9b8a1f4f_fk_sip_profile_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY sofia_gateway
    ADD CONSTRAINT sofia_gateway_sip_profile_id_9b8a1f4f_fk_sip_profile_id FOREIGN KEY (sip_profile_id) REFERENCES sip_profile(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

