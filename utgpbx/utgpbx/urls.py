"""utgpbx URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from django.contrib import admin
from loginpage import views as loginpg
from dashboard import views as dashboardviews
from backuprestore import views as backup
from blocklist import  views as blocklist
from bridges import  views as bridges
from calllogs import views as call_logs
from callparking import views as call_parking
from callqueues import views as call_queues
from chatlogs import views as chat_logs
from contacts import views as contacts
from devices import views as phone
from dialplans import views as dialplan
from digitalreception import views as ivr
from domains import views as domain
from emailsetup import views as email_setup
from extensions import views as extesnions
from groups import views as groups
from inboundrules import views as inbound_rules
from musiconhold import views as moh
from network import views as network
from outboundrules import views as outbound_rules
from pbxsecurity import views as security_pbx
from pbxsettings import  views  as settings_pbx
from recordings import views as recordings
from ringgroups import views as ringgroups
from appsettings import views as settings_utg
from siptrunks import views as sip
from userprofile import views as profile
from utgclient import views as clients
from utgusers import views as users
from voicemail import views as voicemail
from xmltemplates import views as xml_templates
from conference import views as conf
from crmint import views as crm


urlpatterns = [

    # Authentication urls
    url(r'^admin/', admin.site.urls),
    url(r'^login/$', loginpg.login_view, name='login'),
    url(r'^logout/$', loginpg.logout_view, name='logout'),
    url(r'^resetpwd/$', loginpg.passreset_view, name='resetpwd'),

    # display urls
    url(r'^dashboard/$', dashboardviews.dashboard_view, name='dasboard'),
    url(r'^backup/$', backup.backup_restore_view, name='backup'),
    url(r'^blocklist/$', blocklist.blocklist_view, name='blocklist'),
    url(r'^bridges/$', bridges.bridges_view, name='bridges'),
    url(r'^calllogs/$', call_logs.calllogs_view, name='calllogs'),
    url(r'^callparking/$', call_parking.callparkinge_view, name='callparking'),
    url(r'^callqueues/$', call_queues.callqueues_view, name='callqueues'),
    url(r'^callparking/$', call_parking.callparkinge_view, name='callparking'),
    url(r'^chatlogs/$', chat_logs.chatlog_view, name='chatlogs'),
    url(r'^conference/$', conf.conference_view, name='conference'),
    url(r'^contacts/$',contacts.contacts_view , name='contacts'),
    url(r'^devices/$',phone.devices_view, name='devices'),
    url(r'^diaplan/$',dialplan.dailplan_view , name='diaplan'),
    url(r'^digital/$',ivr.digital_reception_view , name='digital'),
    url(r'^domains/$',domain.domains_view , name='domains'),
    url(r'^emailsetup/$',email_setup.emailsetup_view , name='emailsetup'),
    url(r'^extensions/$',extesnions.extensions_view , name='extensions'),
    url(r'^get_user/$', extesnions.connect_extensions_view, name='get_user'),
    url(r'^groups/$',groups.groups_view , name='groups'),
    url(r'^inbound/$',inbound_rules.inbound_rules_view , name='inbound'),
    url(r'^add_inbound/$',inbound_rules.add_inbound_rules_view , name='add_inbound'),
    url(r'^moh/$',moh.moh_view , name='moh'),
    url(r'^network/$',network.network_view, name='network'),
    url(r'^outbound/$',outbound_rules.outbound_rules_view, name='outbound'),
    url(r'^security/$',security_pbx.pbx_security_view , name='security'),
    url(r'pbxsettings/$',settings_pbx.pbx_settings_view , name='pbxsettings'),
    url(r'^recordings/$',recordings.recordings_view , name='recordings'),
    url(r'^ringgroups/$',ringgroups.ringgroups_view , name='ringgroups'),
    url(r'^sip/$',sip.sip_trunks_view, name='sip'),
    url(r'^profile/$',profile.user_profiles_view , name='profile'),
    url(r'^clients/$',clients.utg_client_view , name='clients'),
    url(r'^settings/$',settings_utg.settings_view , name='settings'),
    url(r'^users/$',users.utg_users_view , name='users'),
    url(r'^voicemail/$', voicemail.voicemail_view, name='voicemail'),
    url(r'^xmltemplates/$',xml_templates.xml_templates_view , name='xmltemplates'),
    url(r'^view_crm/$', crm.crm_view, name='view_crm'),

    #add urls
    url(r'^add_sip/$', sip.add_sip_trunks_view, name='add_sip'),
    url(r'^add_users/$', users.utg_edit_users_view, name='add_users'),
    url(r'^add_dialplan/$', dialplan.add_dailplan_view, name='add_dialplan'),
    url(r'^add_domain/$', domain.add_domains_view, name='add_domain'),
    url(r'^add_crm/$', crm.add_crm_view, name='add_crm'),
    url(r'^add_backup/$', backup.zip_folder, name='add_backup'),
    url(r'^add_blocklist/$', blocklist.add_blocklist_view, name='add_blocklist'),
    url(r'^add_callqueues/$', call_queues.add_callqueues_view, name='add_callqueues'),
    url(r'^add_contacts/$', contacts.add_contacts_view, name='add_contacts'),
    url(r'^add_devices/$', phone.add_devices_view, name='add_devices'),
    url(r'^add_digital/$', ivr.add_digital_reception_view, name='add_digital'),
    url(r'^add_extensions/$', extesnions.add_extensions_view, name='add_extensions'),
    url(r'^add_groups/$', groups.add_groups_view, name='add_groups'),
    url(r'^add_outbound/$', outbound_rules.add_outbound_rules_view, name='add_outbound'),
    url(r'^add_ringgroups/$', ringgroups.add_ringgroups_view, name='add_ringgroups'),

    #edit url
    url(r'^edit_users/(?P<username>\d+)$', users.update_user_view, name='edit_users'),
    url(r'^edit_dialplan/(?P<id>\d+)$', dialplan.update_dialplan_view, name='edit_dialplan'),
    url(r'^edit_domain/(?P<id>\d+)$', domain.edit_domains_view, name='edit_domain'),
    url(r'^edit_device/(?P<id>\d+)$', phone.update_device_view, name='edit_device'),
    url(r'^edit_contact/(?P<id>\d+)$', contacts.edit_contacts_view, name='edit_contact'),
    url(r'^edit_sip/(?P<id>\d+)$', sip.edit_sip_trunks_view, name='edit_sip'),
    url(r'^edit_inbound/(?P<id>\d+)$', inbound_rules.edit_inbound_rules_view, name='edit_inbound'),
    url(r'^edit_outbound/(?P<id>\d+)$', outbound_rules.edit_outbound_rules_view, name='edit_outbound'),
    url(r'^edit_digital/(?P<id>\d+)$', ivr.edit_digital_reception_view, name='edit_digital'),
    url(r'^edit_ringgroup/(?P<id>\d+)$', ringgroups.edit_ringgroups_view, name='edit_ringgroup'),
    url(r'^edit_callqueue/(?P<id>\d+)$', call_queues.edit_callqueue_view, name='edit_callqueue'),

    #delete url
    url(r'^delete_users/(?P<username>\d+)$', users.utg_users_delete, name='delete_users'),
    url(r'^delete_dialplan/(?P<id>\d+)$', dialplan.dailplan_delete, name='delete_dialplan'),
    url(r'^delete_domain/(?P<id>\d+)$', domain.utg_domains_delete, name='delete_domain'),
    url(r'^delete_device/(?P<id>\d+)$', phone.utg_device_delete, name='delete_device'),
    url(r'^delete_contact/(?P<id>\d+)$', contacts.utg_contact_delete, name='delete_contact'),
    url(r'^delete_sip/(?P<id>\d+)$', sip.utg_sip_delete, name='delete_sip'),
    url(r'^delete_inbound/(?P<id>\d+)$', inbound_rules.utg_inbound_delete, name='delete_inbound'),
    url(r'^delete_outbound/(?P<id>\d+)$', outbound_rules.utg_outbound_delete, name='delete_outbound'),
    url(r'^delete_digital/(?P<id>\d+)$', ivr.utg_digital_delete, name='delete_digital'),
    url(r'^delete_ringroup/(?P<id>\d+)$', ringgroups.utg_ringgroup_delete, name='delete_ringroup'),
    url(r'^delete_callqueue/(?P<id>\d+)$', call_queues.utg_callqueue_delete, name='delete_callqueue'),



]
