# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.contrib.auth.decorators import login_required

# Create your views here.
from emailsetup.email_add_form import EmailSettingsForm
from emailsetup.models import EmailSetup


@login_required(login_url='/login/')
def emailsetup_view(request, *args, **kwargs):
    title = "Add"
    if request.method == 'POST':
        form = EmailSettingsForm(request.POST)
        if form.is_valid():
            smtp_host = form.cleaned_data.get('smtp_host')
            port = form.cleaned_data.get("port")
            user_name = form.cleaned_data.get('user_name')
            password = form.cleaned_data.get('password')
            template_name = form.cleaned_data.get('template_name')
            from_field = form.cleaned_data.get('from_field')
            subject = form.cleaned_data.get('subject')
            body = form.cleaned_data.get('body')

            usr = EmailSetup()
            usr.smtp_host=smtp_host
            usr.port=port
            usr.user_name=user_name
            usr.password=password
            usr.template_name=template_name
            usr.from_field = from_field
            usr.subject = subject
            usr.body = body
            usr.save()
    else:
        emailsettingobj = EmailSetup.objects.first()
        if emailsettingobj :
            form = EmailSettingsForm(instance=emailsettingobj)
        else:
            form = EmailSettingsForm()

    template = 'email_setup.html'
    return render(request, template, {"form": form, "title": title})
