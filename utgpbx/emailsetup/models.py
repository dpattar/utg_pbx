# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class EmailSetup(models.Model):
    smtp_host = models.CharField(primary_key=True, max_length=500)
    port = models.BigIntegerField(blank=True, null=True)
    user_name = models.CharField(max_length=500, blank=True, null=True)
    password = models.CharField(max_length=500, blank=True, null=True)
    template_name = models.CharField(max_length=255, blank=True, null=True)
    from_field = models.CharField(max_length=255, blank=True,null=True)
    subject = models.CharField(max_length=500, blank=True, null=True)
    body = models.CharField(max_length=5000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'email_setup'

