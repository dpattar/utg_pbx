from django import forms
from domains.models import Domains
from emailsetup.models import  EmailSetup


class EmailSettingsForm(forms.ModelForm):
    smtp_host = forms.CharField(max_length=255)
    smtp_host.widget.attrs.update({'class': 'form-control','placeholder':'smtp.example.com'})
    port = forms.CharField(max_length=255)
    port.widget.attrs.update({'class': 'form-control','placeholder':'587'})
    user_name = forms.CharField(max_length=255)
    user_name.widget.attrs.update({'class': 'form-control','placeholder':'example@exaple.com'})
    password = forms.CharField(max_length=255)
    password.widget.attrs.update({'class': 'form-control', 'placeholder': '********'})
    template_name = forms.CharField(max_length=255)
    template_name.widget.attrs.update({'class': 'form-control', 'placeholder': 'Steve'})
    from_field = forms.CharField(max_length=255)
    from_field.widget.attrs.update({'class': 'form-control', 'placeholder': 'Description'})
    subject = forms.CharField(max_length=500)
    subject.widget.attrs.update({'class': 'form-control', 'placeholder': 'Subject'})
    body = forms.CharField(max_length=2500, widget=forms.Textarea)
    body.widget.attrs.update({'class': 'form-control', 'placeholder': 'Body of email max 2000 characters'})

    class Meta:
        model = EmailSetup
        fields = ['smtp_host', 'port', 'user_name', 'password','template_name', 'from_field', 'subject', 'body']




