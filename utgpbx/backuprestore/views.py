# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.http import HttpResponseRedirect
from os import listdir
from os.path import isfile, join
from os import walk
# Create your views here.
from backuprestore.models import BackupTable
import zipfile
import sys
import os

@login_required(login_url='/login/')
def backup_restore_view(request, *args, **kwargs):
    bkp_obj=BackupTable.objects.all()
    output_path = r'/usr/local/backup/'
    path = "/usr/local/UTG/conf/"  # insert the path to your directory
    rec_obj = os.listdir(path)
    context=locals()
    f = []
    for (dirpath, dirnames, filenames) in walk(output_path):
        f.extend(filenames)
        break
    template = 'backup_restore.html'
    return render(request, template, {'bkp_obj': rec_obj})

@login_required(login_url='/login/')
def zip_folder(request):
    """Zip the contents of an entire folder (with that folder included
    in the archive). Empty subfolders will be included in the archive
    as well.
    """

    input_path=r'/usr/local/UTG/conf/'
    output_path=r'/usr/local/backup/conf.zip'
    parent_folder = os.path.dirname(input_path)
    # Retrieve the paths of the folder contents.
    contents = os.walk(input_path)
    zip_file = zipfile.ZipFile(output_path, 'w', zipfile.ZIP_DEFLATED)
    try:
        zip_file = zipfile.ZipFile(output_path, 'w', zipfile.ZIP_DEFLATED)
        for root, folders, files in contents:
            # Include all subfolders, including empty ones.
            for folder_name in folders:
                absolute_path = os.path.join(root, folder_name)
                relative_path = absolute_path.replace(parent_folder + '\\',
                                                      '')
                print "Adding '%s' to archive." % absolute_path
                zip_file.write(absolute_path, relative_path)
            for file_name in files:
                absolute_path = os.path.join(root, file_name)
                relative_path = absolute_path.replace(parent_folder + '\\',
                                                      '')
                print "Adding '%s' to archive." % absolute_path
                zip_file.write(absolute_path, relative_path)
        print "'%s' created successfully." % output_path
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
    except IOError, message:
        print message
        sys.exit(1)
    except OSError, message:
        print message
        sys.exit(1)
    except zipfile.BadZipfile, message:
        print message
        sys.exit(1)
    finally:
        zip_file.close()