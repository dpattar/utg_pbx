# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class BackupTable(models.Model):
    id = models.BigIntegerField(primary_key=True)
    file_name = models.CharField(max_length=255)
    date = models.DateField()
    version = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'backup_table'