# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from utgusers.models import AuthUser

class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.BigIntegerField(blank=True, null=True)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=254)
    is_admin = models.NullBooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField(blank=True, null=True)
    is_user = models.NullBooleanField()
    is_agent = models.NullBooleanField()
    is_staff = models.NullBooleanField()
    is_enabled = models.NullBooleanField()
    user_group=models.SmallIntegerField()
    mobile=models.BigIntegerField(blank=True, null=True)
    outbound_caller_id=models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'auth_user'

    def __unicode__(self):
        return str(self.username)

# Create your models here.
class CallQueues(models.Model):
    id = models.BigIntegerField(primary_key=True)
    extension = models.BigIntegerField(blank=True, null=True)
    poll_strategy = models.CharField(max_length=255, blank=True, null=True)
    timeout = models.BigIntegerField(blank=True, null=True)
    queue_name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'call_queues'


class AgentDetails(models.Model):
    agent = models.ForeignKey('AuthUser', models.DO_NOTHING)
    tier_level = models.BigIntegerField(blank=True, null=True)
    tier_posotion = models.BigIntegerField(blank=True, null=True)
    queue_name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'agent_details'

class Strategy(models.Model):
    strategy = models.CharField(db_column='Strategy', primary_key=True, max_length=255)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Strategy'

    def __unicode__(self):
        return str(self.strategy)

class CallQueueUsers(models.Model):

    call_queue = models.BigIntegerField(blank=True, null=True)
    user = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'call_queue_users'