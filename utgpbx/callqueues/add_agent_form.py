from django import forms
from siptrunks.models import SipTrunks

class CallqueueAgentAddForm(forms.Form):
    agent = forms.CharField(max_length=255)
    agent.widget.attrs.update({'class': 'form-control'})

    tierlevel = forms.CharField(max_length=255)
    tierlevel.widget.attrs.update({'class': 'form-control'})

    queue_id = forms.CharField(max_length=255)
    queue_id.widget.attrs.update({'class': 'form-control'})

    position = forms.CharField(max_length=255)
    position.widget.attrs.update({'class': 'form-control'})

