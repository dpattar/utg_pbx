# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.db.models import Max
from django.contrib.auth.decorators import login_required
# Create your views here.
from callqueues.add_agent_form import CallqueueAgentAddForm
from callqueues.call_queues_form import CallqueueAddForm
from callqueues.models import CallQueues, AgentDetails, CallQueueUsers
from ringgroups.models import RinggroupUser
from django.template import RequestContext, Context, loader
import csv

call_queue_name=''

@login_required(login_url='/login/')
def callqueues_view(request, *args, **kwargs):
    call_que_obj=CallQueues.objects.all()
    context=locals()
    template = 'callqueues.html'
    return render(request, template, {'call_que_obj' : call_que_obj})

def utg_callqueue_delete(request, id, *args, **kwargs):
    object_users_model1 = CallQueues.objects.get(id=id)
    #object_group_model = RinggroupUser.objects.get(ringgroup_id=id).delete()
    object_users_model1.delete()
    CallQueueUsers.objects.filter(call_queue=id).delete()
    # for rgp in object_group_model :
    #     rgp.delete()
    object_users_model = CallQueues.objects.all()
    template = 'callqueues.html'
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


@login_required(login_url='/login/')
def add_callqueues_view(request, *args, **kwargs):
    ids=CallQueues.objects.all().aggregate(Max('id'))
    id=ids.get('id', 0)

    title = "Add"
    if request.method == 'POST':
        form = CallqueueAddForm(request.POST)
        if form.is_valid():
            # form.save(commit=True)
            name=form.cleaned_data.get('queue_name')
            extension = form.cleaned_data.get("extension")
            poll_strategy = form.cleaned_data.get('poll_strategy')
            timeout = form.cleaned_data.get('timeout')
            user = form.cleaned_data.get('users')
            usr = CallQueues()
            usr.id=id+1
            usr.extension = extension
            usr.poll_strategy = poll_strategy.strategy
            usr.timeout = timeout
            usr.queue_name=name
            usr.save()
            for usr in user :
                idsm = CallQueueUsers.objects.all().aggregate(Max('id'))
                idm = idsm.get('id', 0)
                usrg = CallQueueUsers()
                usrg.id=idm
                usrg.call_queue = id + 1
                usrg.user = usr.username
                usrg.save()
            obj = {'name': name, 'poll_strategy': poll_strategy.strategy, 'timeout': timeout,
                   'usrg': user,'extension':extension}
            connect_call_queues_view(request, name, obj)
            create_que_exgtension(request, name, obj)
            #return render(request, 'user_test.html', {"user": user})

            # return HttpResponseRedirect('/add_callqueue_agent/')
            return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
            #return render(request, 'add_agents.html', {'id':id,'name':name,'count':agent_count})
            # form.save()
        else:
            print form.errors
    else:
        form = CallqueueAddForm()
    template = 'add_call_queue.html'
    return render(request, template, {"form": form, "title": title})


@login_required(login_url='/login/')
def edit_callqueue_view(request,id, *args, **kwargs):
    title = "Add"
    if request.method == 'POST':
        form = CallqueueAddForm(request.POST)
        if form.is_valid():
            # form.save(commit=True)
            name = form.cleaned_data.get('queue_name')
            extension = form.cleaned_data.get("extension")
            poll_strategy = form.cleaned_data.get('poll_strategy')
            timeout = form.cleaned_data.get('timeout')
            user = form.cleaned_data.get('users')
            usr = CallQueues.objects.get(id=id)
            usr.extension = extension
            usr.poll_strategy = poll_strategy.strategy
            usr.timeout = timeout
            usr.queue_name = name
            usr.save()
            for usr in user:
                usrg = CallQueueUsers()
                usrg.call_queue = id
                usrg.user = usr.username
                usrg.save()
            return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
            # return render(request, 'utgusers.html', {})
            # form.save()
        else:
            print form.errors
    else:
        sip_model = CallQueues.objects.get(id=id)
        #users=CallQueueUsers.objects.get(call_queue=id)
        form = CallqueueAddForm(instance=sip_model)

    template = 'add_call_queue.html'
    return render(request, template, {"form": form, "title": title})


def connect_call_queues_view(request,name, obj):
    try:
        t = loader.get_template('call_queue.xml')
    except:
        print(request, """customer extension config xml file update failed.
                   Can not load template file !""")

    try:
        path = '/usr/local/UTG/autoload_configs/callcenter.conf.xml'
        f = open(path, 'w')
        try:
            f.write(t.render(obj))
            f.close()
        except:
            print(request, """customer sip config xml file update failed.
                     Can not load template file !""")
    finally:
        print(request, """customer sip config xml file update failed.
                            Can not load template file !""")

    return True

def create_que_exgtension(request,name, obj):
    try:
        t = loader.get_template('dialplan_que.xml')
    except:
        print(request, """customer extension config xml file update failed.
                   Can not load template file !""")

    try:
        path = '//usr/local/UTG/conf/dialplan/default/' + name + '.xml'
        f = open(path, 'w')
        try:
            f.write(t.render(obj))
            f.close()
        except:
            print(request, """customer sip config xml file update failed.
                     Can not load template file !""")
    finally:
        print(request, """customer sip config xml file update failed.
                            Can not load template file !""")

    return True

