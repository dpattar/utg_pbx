from django import forms

from callqueues.models import CallQueues, AuthUser, Strategy
from siptrunks.models import SipTrunks

class CallqueueAddForm(forms.ModelForm):
    queue_name = forms.CharField(max_length=255)
    queue_name.widget.attrs.update({'class': 'form-control'})

    extension = forms.CharField(max_length=255)
    extension.widget.attrs.update({'class': 'form-control'})

    poll_strategy = forms.ModelChoiceField(queryset=Strategy.objects.all())
    poll_strategy.widget.attrs.update({'class': 'form-control'})

    timeout = forms.CharField(max_length=255)
    timeout.widget.attrs.update({'class': 'form-control'})

    users = forms.ModelMultipleChoiceField(queryset=AuthUser.objects.all())
    users.widget.attrs.update({'class': 'form-control js-example-basic-multiple select2-container','multiple':"multiple"})

    class Meta:
        model = CallQueues
        fields = ['queue_name', 'extension', 'poll_strategy', 'timeout']