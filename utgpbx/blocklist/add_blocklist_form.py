from django import forms
from contacts.models import Contacts

class BlacklistAddForm(forms.Form):
    user = forms.CharField(max_length=255)
    user.widget.attrs.update({'class': 'form-control'})

    description = forms.CharField(max_length=255)
    description.widget.attrs.update({'class': 'form-control'})


