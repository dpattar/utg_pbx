# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.db.models import Max

# Create your views here.
from blocklist.add_blocklist_form import BlacklistAddForm
from blocklist.models import BlaclistedTables
from django.contrib.auth.decorators import login_required

@login_required(login_url='/login/')
def blocklist_view(request, *args, **kwargs):
    blk_obj=BlaclistedTables.objects.all()
    context=locals()
    template = 'blocklist.html'
    return render(request, template, {'blk_obj': blk_obj})


@login_required(login_url='/login/')
def add_blocklist_view(request, *args, **kwargs):
    ids = BlaclistedTables.objects.all().aggregate(Max('id'))
    id = ids.get('id', 1)
    title = "Add"
    if request.method == 'POST':
        form = BlacklistAddForm(request.POST)
        if form.is_valid():
            # form.save(commit=True)
            user = form.cleaned_data.get("user")
            description = form.cleaned_data.get('description')


            usr = BlaclistedTables()
            usr.id=id+1
            usr.user = user
            usr.description = description
            usr.save()

            return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
            # return render(request, 'utgusers.html', {})
            # form.save()
        else:
            print form.errors
    else:
        form = BlacklistAddForm()
    template = 'add_blocklist.html'
    return render(request, template, {"form": form, "title": title})

