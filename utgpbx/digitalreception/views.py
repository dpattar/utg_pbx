# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.template import RequestContext, Context, loader

# Create your views here.
from digitalreception.digital_add_form import DigiRecAddForm
from digitalreception.models import DigitalReceptionist
from django.db.models import Max
from django.contrib.auth.decorators import login_required
from xmlrpclib import ServerProxy

@login_required(login_url='/login/')
def digital_reception_view(request, *args, **kwargs):
    rec_obj = DigitalReceptionist.objects.all()
    context = locals()
    template = 'digital_reception.html'
    return render(request, template, {'rec_obj': rec_obj})


def utg_digital_delete(request, id, *args, **kwargs):
    object_users_model1 = DigitalReceptionist.objects.get(id=id)
    object_users_model1.delete()
    object_users_model = DigitalReceptionist.objects.all()
    template = 'digital_reception.html'
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


@login_required(login_url='/login/')
def add_digital_reception_view(request, *args, **kwargs):
    ids = DigitalReceptionist.objects.all().aggregate(Max('id'))
    id = ids.get('id', 1)
    title = "Add"


    rpc_host = 'localhost'
    rpc_username = 'freeswitch'
    rpc_password = 'works'
    rpc_port = '8080'

    if request.method == 'POST':
        form = DigiRecAddForm(request.POST)
        if form.is_valid():
            # form.save(commit=True)
            name = form.cleaned_data.get("name")
            prompt = form.cleaned_data.get('prompt')
            extension = form.cleaned_data.get('extension')
            action_0 = form.cleaned_data.get('action_0')
            action_1 = form.cleaned_data.get('action_1')
            action_2 = form.cleaned_data.get('action_2')
            action_3 = form.cleaned_data.get('action_3')
            action_4 = form.cleaned_data.get('action_4')
            action_5 = form.cleaned_data.get('action_5')
            action_6 = form.cleaned_data.get('action_6')
            action_7 = form.cleaned_data.get('action_7')
            action_8 = form.cleaned_data.get('action_8')
            action_9 = form.cleaned_data.get('action_9')

            ac_value_0 = form.cleaned_data.get('ac_value_0')
            ac_value_1 = form.cleaned_data.get('ac_value_1')
            ac_value_2 = form.cleaned_data.get('ac_value_2')
            ac_value_3 = form.cleaned_data.get('ac_value_3')
            ac_value_4 = form.cleaned_data.get('ac_value_4')
            ac_value_5 = form.cleaned_data.get('ac_value_5')
            ac_value_6 = form.cleaned_data.get('ac_value_6')
            ac_value_7 = form.cleaned_data.get('ac_value_7')
            ac_value_8 = form.cleaned_data.get('ac_value_8')
            ac_value_9 = form.cleaned_data.get('ac_value_9')

            prompt_ac_value_0 = form.cleaned_data.get('prompt_ac_value_0')
            prompt_ac_value_1 = form.cleaned_data.get('prompt_ac_value_1')
            prompt_ac_value_2 = form.cleaned_data.get('prompt_ac_value_2')
            prompt_ac_value_3 = form.cleaned_data.get('prompt_ac_value_3')
            prompt_ac_value_4 = form.cleaned_data.get('prompt_ac_value_4')
            prompt_ac_value_5 = form.cleaned_data.get('prompt_ac_value_5')
            prompt_ac_value_6 = form.cleaned_data.get('prompt_ac_value_6')
            prompt_ac_value_7 = form.cleaned_data.get('prompt_ac_value_7')
            prompt_ac_value_8 = form.cleaned_data.get('prompt_ac_value_8')
            prompt_ac_value_9 = form.cleaned_data.get('prompt_ac_value_9')

            inactive_second = form.cleaned_data.get('inactive_second')
            inactive_action = form.cleaned_data.get('inactive_action')
            invalid_action = form.cleaned_data.get('invalid_action')

            usr = DigitalReceptionist()
            usr.id = id + 1
            usr.name = name
            usr.prompt = prompt
            usr.extension = extension
            usr.action_0 = action_0.action
            usr.action_1 = action_1.action
            usr.action_2 = action_2.action
            usr.action_3 = action_3.action
            usr.action_4 = action_4.action
            usr.action_5 = action_5.action
            usr.action_6 = action_6.action
            usr.action_7 = action_7.action
            usr.action_8 = action_8.action
            usr.action_9 = action_9.action

            usr.ac_value_0 = ac_value_0
            usr.ac_value_1 = ac_value_1
            usr.ac_value_2 = ac_value_2
            usr.ac_value_3 = ac_value_3
            usr.ac_value_4 = ac_value_4
            usr.ac_value_5 = ac_value_5
            usr.ac_value_6 = ac_value_6
            usr.ac_value_7 = ac_value_7
            usr.ac_value_8 = ac_value_8
            usr.ac_value_9 = ac_value_9

            usr.inactive_second = inactive_second
            usr.inactive_action = inactive_action.action
            usr.invalid_action = invalid_action.action

            usr.save()
            obj = {'name': name, 'extension': extension, 'prompt':prompt,'action_0': action_0.action, 'action_1': action_1.action,
                   'action_2': action_2.action, 'action_3': action_3.action, 'action_4': action_4.action,
                   'action_5': action_5.action, 'action_6': action_6.action, 'action_7': action_7.action,
                   'action_8': action_8.action, 'action_9': action_9.action,
                   'ac_value_0': ac_value_0,
                   'ac_value_1': ac_value_1,
                   'ac_value_2': ac_value_2,
                   'ac_value_3': ac_value_3,
                   'ac_value_4': ac_value_4,
                   'ac_value_5': ac_value_5,
                   'ac_value_6': ac_value_6,
                   'ac_value_7': ac_value_7,
                   'ac_value_8': ac_value_8,
                   'ac_value_9': ac_value_9,

                   'prompt_ac_value_0': prompt_ac_value_0,
                   'prompt_ac_value_1': prompt_ac_value_1,
                   'prompt_ac_value_2': prompt_ac_value_2,
                   'prompt_ac_value_3': prompt_ac_value_3,
                   'prompt_ac_value_4': prompt_ac_value_4,
                   'prompt_ac_value_5': prompt_ac_value_5,
                   'prompt_ac_value_6': prompt_ac_value_6,
                   'prompt_ac_value_7': prompt_ac_value_7,
                   'prompt_ac_value_8': prompt_ac_value_8,
                   'prompt_ac_value_9': prompt_ac_value_9,
                   'inactive_second':inactive_second,'inactive_action':inactive_action.action,
                   'invalid_action':invalid_action.action}
            connect_digital_rules_view(request, name, obj)
            connect_digital_voice_view(request, name, obj)
            server = ServerProxy("http://%s:%s@%s:%s" % (rpc_username, rpc_password, rpc_host, rpc_port))
            server.freeswitch.api("reloadxml", "")
            return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
            # return render(request, 'utgusers.html', {})
            # form.save()
        else:
            print form.errors
    else:
        form = DigiRecAddForm()
    template = 'add_didgital_rec.html'
    return render(request, template, {"form": form, "title": title})


@login_required(login_url='/login/')
def edit_digital_reception_view(request, id, *args, **kwargs):
    title = "Add"
    if request.method == 'POST':
        form = DigiRecAddForm(request.POST)
        if form.is_valid():
            # form.save(commit=True)
            name = form.cleaned_data.get("name")
            prompt = form.cleaned_data.get('prompt')
            extension = form.cleaned_data.get('extension')
            action_0 = form.cleaned_data.get('action_0')
            action_1 = form.cleaned_data.get('action_1')
            action_2 = form.cleaned_data.get('action_2')
            action_3 = form.cleaned_data.get('action_3')
            action_4 = form.cleaned_data.get('action_4')
            action_5 = form.cleaned_data.get('action_5')
            action_6 = form.cleaned_data.get('action_6')
            action_7 = form.cleaned_data.get('action_7')
            action_8 = form.cleaned_data.get('action_8')
            action_9 = form.cleaned_data.get('action_9')

            ac_value_0 = form.cleaned_data.get('ac_value_0')
            ac_value_1 = form.cleaned_data.get('ac_value_1')
            ac_value_2 = form.cleaned_data.get('ac_value_2')
            ac_value_3 = form.cleaned_data.get('ac_value_3')
            ac_value_4 = form.cleaned_data.get('ac_value_4')
            ac_value_5 = form.cleaned_data.get('ac_value_5')
            ac_value_6 = form.cleaned_data.get('ac_value_6')
            ac_value_7 = form.cleaned_data.get('ac_value_7')
            ac_value_8 = form.cleaned_data.get('ac_value_8')
            ac_value_9 = form.cleaned_data.get('ac_value_9')

            inactive_second = form.cleaned_data.get('inactive_second')
            inactive_action = form.cleaned_data.get('inactive_action')
            invalid_action = form.cleaned_data.get('invalid_action')

            usr = DigitalReceptionist()

            usr.name = name
            usr.prompt = prompt._get_name()
            usr.extension = extension
            usr.action_0 = action_0.action
            usr.action_1 = action_1.action
            usr.action_2 = action_2.action
            usr.action_3 = action_3.action
            usr.action_4 = action_4.action
            usr.action_5 = action_5.action
            usr.action_6 = action_6.action
            usr.action_7 = action_7.action
            usr.action_8 = action_8.action
            usr.action_9 = action_9.action

            usr.ac_value_0 = ac_value_0
            usr.ac_value_1 = ac_value_1
            usr.ac_value_2 = ac_value_2
            usr.ac_value_3 = ac_value_3
            usr.ac_value_4 = ac_value_4
            usr.ac_value_5 = ac_value_5
            usr.ac_value_6 = ac_value_6
            usr.ac_value_7 = ac_value_7
            usr.ac_value_8 = ac_value_8
            usr.ac_value_9 = ac_value_9

            usr.inactive_second = inactive_second
            usr.inactive_action = inactive_action.action
            usr.invalid_action = invalid_action.action

            usr.save()

            return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
            # return render(request, 'utgusers.html', {})
            # form.save()
        else:
            print form.errors
    else:
        domain_obj = DigitalReceptionist.objects.get(id=id)
        form = DigiRecAddForm(instance=domain_obj)
    template = 'add_didgital_rec.html'
    return render(request, template, {"form": form, "title": title})


def connect_digital_rules_view(request, name, obj):
    try:
        t = loader.get_template('ivr_demo.xml')
    except:
        print(request, """customer extension config xml file update failed.
                   Can not load template file !""")

    try:
        path = '/usr/local/UTG/conf/dialplan/default/' + name + '.xml'
        f = open(path, 'w')
        try:
            f.write(t.render(obj))
            f.close()
        except:
            print(request, """customer sip config xml file update failed.
                     Can not load template file !""")
    finally:
        print(request, """customer sip config xml file update failed.
                            Can not load template file !""")

    return True

def connect_digital_voice_view(request, name, obj):
    try:
        t = loader.get_template('ivr.lua')
    except:
        print(request, """customer extension config xml file update failed.
                   Can not load template file !""")

    try:
        path = '//usr/local/UTG/scripts/' + name + '.lua'
        f = open(path, 'w')
        try:
            f.write(t.render(obj))
            f.close()
        except:
            print(request, """customer sip config xml file update failed.
                     Can not load template file !""")
    finally:
        print(request, """customer sip config xml file update failed.
                            Can not load template file !""")

    return True
