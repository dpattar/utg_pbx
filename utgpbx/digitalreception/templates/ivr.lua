session:answer();
while (session:ready() == true) do
    session:setAutoHangup(false);
    session:set_tts_params("flite", "slt");
    session:speak("{{ prompt }}");
    session:sleep(50);
    session:speak("please select an Option.");
    session:sleep(100);

    {% if  ac_value_0 != None %}
    session:speak("to call {{ prompt_ac_value_0 }}, press 0");
    session:sleep(100);
    {% endif %}

    {% if  ac_value_1 != None %}
    session:speak("to call {{ prompt_ac_value_1 }}, press 1");
    session:sleep(100);
    {% endif %}
    {% if  ac_value_2 != None %}
    session:speak("to Call {{ prompt_ac_value_2 }}, press 2");
    session:sleep(100);
    {% endif %}
    {% if  ac_value_3 != None %}
    session:speak("to Call {{ prompt_ac_value_3 }}, press 3");
    session:sleep(100);
    {% endif %}
    {% if  ac_value_4 != None %}
    session:speak("to call {{ prompt_ac_value_4 }}, press 4");
    session:sleep(100);
    {% endif %}
    {% if  ac_value_5 != None %}
     session:speak("to call {{ prompt_ac_value_5 }}, press 5");
    session:sleep(100);
    {% endif %}
    {% if  ac_value_6 != None %}
     session:speak("to call {{ prompt_ac_value_6 }}, press 6");
    session:sleep(100);
    {% endif %}
    {% if  ac_value_7 != None %}
     session:speak("to call {{ prompt_ac_value_7 }}, press 7");
    session:sleep(100);
    {% endif %}
    {% if  ac_value_8 != None %}
     session:speak("to call {{ prompt_ac_value_8 }}, press 8");
    session:sleep(100);
    {% endif %}
    {% if  ac_value_9 != None %}
     session:speak("to call {{ prompt_ac_value_9 }}, press 9");
    session:sleep(100);
    {% endif %}



    session:sleep(3000);
    digits = session:getDigits(1, "", 3000);

       {% if  ac_value_0 != None %}
    if (digits == "0")  then
        session:execute("transfer","{{ ac_value_0 }}");
    end
       {% endif %}

    {% if  ac_value_1 != None %}
    if (digits == "1")  then
        session:execute("transfer","{{ ac_value_1 }}");
    end
    {% endif %}
    {% if  ac_value_2 != None %}
    if (digits == "2")  then
        session:execute("transfer","{{ ac_value_2 }}");
    end
       {% endif %}
    {% if  ac_value_3 != None %}
    if (digits == "3")  then
        session:execute("transfer","{{ ac_value_3 }}");
    end
       {% endif %}
    {% if  ac_value_4 != None %}
    if (digits == "4")  then
        session:execute("transfer","{{ ac_value_4 }}");
    end
       {% endif %}
    {% if  ac_value_5 != None %}
    if (digits == "5")  then
        session:execute("transfer","{{ ac_value_5 }}");
    end
       {% endif %}
    {% if  ac_value_6 != None %}
    if (digits == "6")  then
        session:execute("transfer","{{ ac_value_6 }}");
    end
       {% endif %}
    {% if  ac_value_7 != None %}
    if (digits == "7")  then
        session:execute("transfer","{{ ac_value_7 }}");
    end
       {% endif %}
    {% if  ac_value_8 != None %}
    if (digits == "8")  then
        session:execute("transfer","{{ ac_value_8 }}");
    end
       {% endif %}
    {% if  ac_value_9 != None %}
    if (digits == "9")  then
        session:execute("transfer","{{ ac_value_9 }}");
    end
       {% endif %}


end