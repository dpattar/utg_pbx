# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class DigitalReceptionist(models.Model):
    id = models.BigIntegerField(primary_key=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    prompt = models.CharField(max_length=255, blank=True, null=True)
    extension = models.BigIntegerField(blank=True, null=True)
    action_0 = models.CharField(max_length=255, blank=True, null=True)
    action_1 = models.CharField(max_length=255, blank=True, null=True)
    action_2 = models.CharField(max_length=255, blank=True, null=True)
    action_3 = models.CharField(max_length=255, blank=True, null=True)
    action_4 = models.CharField(max_length=255, blank=True, null=True)
    action_5 = models.CharField(max_length=255, blank=True, null=True)
    action_6 = models.CharField(max_length=255, blank=True, null=True)
    action_7 = models.CharField(max_length=255, blank=True, null=True)
    action_8 = models.CharField(max_length=255, blank=True, null=True)
    action_9 = models.CharField(max_length=255, blank=True, null=True)
    ac_value_1 = models.CharField(max_length=255, blank=True, null=True)
    ac_value_2 = models.CharField(max_length=255, blank=True, null=True)
    ac_value_3 = models.CharField(max_length=255, blank=True, null=True)
    ac_value_4 = models.CharField(max_length=255, blank=True, null=True)
    ac_value_5 = models.CharField(max_length=255, blank=True, null=True)
    ac_value_6 = models.CharField(max_length=255, blank=True, null=True)
    ac_value_7 = models.CharField(max_length=255, blank=True, null=True)
    ac_value_8 = models.CharField(max_length=255, blank=True, null=True)
    ac_value_9 = models.CharField(max_length=255, blank=True, null=True)
    inactive_second = models.BigIntegerField(blank=True, null=True)
    inactive_action = models.CharField(max_length=255, blank=True, null=True)
    invalid_action = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'digital_receptionist'

class DigitalActions(models.Model):
    action = models.CharField(primary_key=True, max_length=255)

    class Meta:
        managed = False
        db_table = 'digital_actions'

    def __unicode__(self):
        return str(self.action)
