from django import forms


from digitalreception.models import DigitalReceptionist, DigitalActions
from siptrunks.models import SipTrunks

class DigiRecAddForm(forms.ModelForm):
    name = forms.CharField(max_length=255)
    name.widget.attrs.update({'class': 'form-control'})

    extension = forms.CharField(max_length=255)
    extension.widget.attrs.update({'class': 'form-control'})

    prompt = forms.CharField(max_length=255,required=False)
    prompt.widget.attrs.update({'class': 'form-control'})

    action_0 = forms.ModelChoiceField(queryset=DigitalActions.objects.all(),initial=0)
    action_0.widget.attrs.update({'class': 'form-control'})

    action_1 = forms.ModelChoiceField(queryset=DigitalActions.objects.all(),initial=0)
    action_1.widget.attrs.update({'class': 'form-control'})

    action_2 =forms.ModelChoiceField(queryset=DigitalActions.objects.all(),initial=0)
    action_2.widget.attrs.update({'class': 'form-control'})

    action_3 = forms.ModelChoiceField(queryset=DigitalActions.objects.all(),initial=0)
    action_3.widget.attrs.update({'class': 'form-control'})

    action_4 = forms.ModelChoiceField(queryset=DigitalActions.objects.all(),initial=0)
    action_4.widget.attrs.update({'class': 'form-control'})

    action_5 = forms.ModelChoiceField(queryset=DigitalActions.objects.all(),initial=0)
    action_5.widget.attrs.update({'class': 'form-control'})

    action_6 = forms.ModelChoiceField(queryset=DigitalActions.objects.all(),initial=0)
    action_6.widget.attrs.update({'class': 'form-control'})

    action_7 = forms.ModelChoiceField(queryset=DigitalActions.objects.all(),initial=0)
    action_7.widget.attrs.update({'class': 'form-control'})

    action_8 = forms.ModelChoiceField(queryset=DigitalActions.objects.all(),initial=0)
    action_8.widget.attrs.update({'class': 'form-control'})

    action_9 = forms.ModelChoiceField(queryset=DigitalActions.objects.all(),initial=0)
    action_9.widget.attrs.update({'class': 'form-control'})

    ac_value_0 = forms.CharField(max_length=255,required=False)
    ac_value_0.widget.attrs.update({'class': 'form-control'})
    ac_value_1 = forms.CharField(max_length=255,required=False)
    ac_value_1.widget.attrs.update({'class': 'form-control'})
    ac_value_2 = forms.CharField(max_length=255,required=False)
    ac_value_2.widget.attrs.update({'class': 'form-control'})
    ac_value_3 = forms.CharField(max_length=255,required=False)
    ac_value_3.widget.attrs.update({'class': 'form-control'})
    ac_value_4 = forms.CharField(max_length=255,required=False)
    ac_value_4.widget.attrs.update({'class': 'form-control'})
    ac_value_5 = forms.CharField(max_length=255,required=False)
    ac_value_5.widget.attrs.update({'class': 'form-control'})
    ac_value_6 = forms.CharField(max_length=255,required=False)
    ac_value_6.widget.attrs.update({'class': 'form-control'})
    ac_value_7 = forms.CharField(max_length=255,required=False)
    ac_value_7.widget.attrs.update({'class': 'form-control'})
    ac_value_8 = forms.CharField(max_length=255,required=False)
    ac_value_8.widget.attrs.update({'class': 'form-control'})
    ac_value_9 = forms.CharField(max_length=255,required=False)
    ac_value_9.widget.attrs.update({'class': 'form-control'})

    prompt_ac_value_0 = forms.CharField(max_length=255, required=False)
    prompt_ac_value_0.widget.attrs.update({'class': 'form-control'})
    prompt_ac_value_1 = forms.CharField(max_length=255, required=False)
    prompt_ac_value_1.widget.attrs.update({'class': 'form-control'})
    prompt_ac_value_2 = forms.CharField(max_length=255, required=False)
    prompt_ac_value_2.widget.attrs.update({'class': 'form-control'})
    prompt_ac_value_3 = forms.CharField(max_length=255, required=False)
    prompt_ac_value_3.widget.attrs.update({'class': 'form-control'})
    prompt_ac_value_4 = forms.CharField(max_length=255, required=False)
    prompt_ac_value_4.widget.attrs.update({'class': 'form-control'})
    prompt_ac_value_5 = forms.CharField(max_length=255, required=False)
    prompt_ac_value_5.widget.attrs.update({'class': 'form-control'})
    prompt_ac_value_6 = forms.CharField(max_length=255, required=False)
    prompt_ac_value_6.widget.attrs.update({'class': 'form-control'})
    prompt_ac_value_7 = forms.CharField(max_length=255, required=False)
    prompt_ac_value_7.widget.attrs.update({'class': 'form-control'})
    prompt_ac_value_8 = forms.CharField(max_length=255, required=False)
    prompt_ac_value_8.widget.attrs.update({'class': 'form-control'})
    prompt_ac_value_9 = forms.CharField(max_length=255, required=False)
    prompt_ac_value_9.widget.attrs.update({'class': 'form-control'})

    inactive_second= forms.CharField(max_length=255)
    inactive_second.widget.attrs.update({'class': 'form-control'})
    inactive_action=forms.ModelChoiceField(queryset=DigitalActions.objects.all())
    inactive_action.widget.attrs.update({'class': 'form-control'})
    invalid_action=forms.ModelChoiceField(queryset=DigitalActions.objects.all())
    invalid_action.widget.attrs.update({'class': 'form-control'})

    class Meta:
        model = DigitalReceptionist
        fields = ['name', 'extension', 'prompt','action_0','action_1','action_2','action_3','action_4','action_5',
                  'action_6','action_7','action_8','action_9','ac_value_0','ac_value_1','ac_value_2','ac_value_3'
                  ,'ac_value_4','ac_value_5','ac_value_6','ac_value_7','ac_value_8','ac_value_9','inactive_second','inactive_action'
                  ,'invalid_action']

