from django import forms
from siptrunks.models import SipTrunks

class SipAddForm(forms.ModelForm):
    name = forms.CharField(max_length=255)
    name.widget.attrs.update({'class': 'form-control','placeholder':'Floroute'})

    username = forms.CharField(max_length=255)
    username.widget.attrs.update({'class': 'form-control','placeholder':'GreenLamp1'})

    password = forms.CharField(max_length=255,widget=forms.PasswordInput())
    password.widget.attrs.update({'class': 'form-control','type': 'password','placeholder':'********'})

    host = forms.CharField(max_length=255)
    host.widget.attrs.update({'class': 'form-control','placeholder':'www.flouroute.com'})

    proxy = forms.CharField(max_length=255)
    proxy.widget.attrs.update({'class': 'form-control','placeholder':'example.sip.com'})

    class Meta:
        model = SipTrunks
        fields = ['name', 'username', 'password', 'host', 'proxy']



