# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.template import RequestContext, Context, loader
import os
# Create your views here.
from siptrunks.models import SipTrunks
from siptrunks.sip_add_form import SipAddForm
from django.db.models import Max
from django.contrib.auth.decorators import login_required

@login_required(login_url='/login/')
def sip_trunks_view(request, *args, **kwargs):
    sip_obj=SipTrunks.objects.all()
    context=locals()
    template = 'sip_trunks.html'
    return render(request, template, {'sip_obj' : sip_obj})

def utg_sip_delete(request, id, *args, **kwargs):
    object_users_model1 = SipTrunks.objects.get(id=id)
    object_users_model1.delete()
    object_users_model = SipTrunks.objects.all()
    template = 'utgusers.html'
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))

@login_required(login_url='/login/')
def add_sip_trunks_view(request, *args, **kwargs):
    ids = SipTrunks.objects.all().aggregate(Max('id'))
    id = ids.get('id', 0)
    title = "Add"
    if request.method == 'POST':
        form = SipAddForm(request.POST)
        if form.is_valid():
            # form.save(commit=True)
            name = form.cleaned_data.get("name")
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            host = form.cleaned_data.get('host')
            proxy = form.cleaned_data.get('proxy')

            usr = SipTrunks()
            usr.id = id+1
            usr.name = name
            usr.username = username
            usr.password = password
            usr.host = host
            usr.proxy = proxy
            obj={'name':name,'user_name':username,'password':password,'domain':host,'proxy':proxy}
            usr.save()
            if connect_sip_view(request, name,obj) :
                return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
            else:
                return render(request, 'dashboard.html', {})

            # return render(request, 'utgusers.html', {})
            # form.save()
        else:
            print form.errors
    else:
        form = SipAddForm()
    template = 'add_sip.html'
    return render(request, template, {"form": form, "title": title})


@login_required(login_url='/login/')
def edit_sip_trunks_view(request,id, *args, **kwargs):
    title = "Add"
    if request.method == 'POST':
        form = SipAddForm(request.POST)
        if form.is_valid():
            # form.save(commit=True)
            name = form.cleaned_data.get("name")
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            host = form.cleaned_data.get('host')
            proxy = form.cleaned_data.get('proxy')

            usr = SipTrunks.objects.get(id=id)
            usr.name = name
            usr.username = username
            usr.password = password
            usr.host = host
            usr.proxy = proxy
            obj={'name':name,'user_name':username,'password':password,'domain':host,'proxy':proxy}
            usr.save()
            if connect_sip_view(request, name,obj) :
                return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
            else:
                return render(request, 'dashboard.html', {})

            # return render(request, 'utgusers.html', {})
            # form.save()
        else:
            print form.errors
    else:
        sip_model = SipTrunks.objects.get(id=id)
        form = SipAddForm(instance=sip_model)
    template = 'add_sip.html'
    return render(request, template, {"form": form, "title": title})

def connect_sip_view(request, name,obj):
   try:
        t = loader.get_template('sip.xml')
   except :
       print(request, """customer sip config xml file update failed.
                   Can not load template file !""")

   try:
       path=r'/usr/local/src/UTG/conf/sip_profiles/external/'+name+'.xml'
       f= open(path,'w')
       try:
           f.write(t.render(obj))
           f.close()
       except:
           print(request, """customer sip config xml file update failed.
                     Can not load template file !""")
   finally:
       print(request, """customer sip config xml file update failed.
                            Can not load template file !""")


   return True