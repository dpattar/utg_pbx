# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class SipTrunks(models.Model):
    id = models.BigIntegerField(primary_key=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    host = models.CharField(max_length=255, blank=True, null=True)
    type = models.CharField(max_length=255, blank=True, null=True)
    trunk_no = models.BigIntegerField(blank=True, null=True)
    password = models.CharField(max_length=255, blank=True, null=True)
    user_name = models.CharField(max_length=255, blank=True, null=True)
    proxy = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'sip_trunks'