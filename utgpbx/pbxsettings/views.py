# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.contrib.auth.decorators import login_required
# Create your views here.

@login_required(login_url='/login/')
def pbx_settings_view(request, *args, **kwargs):
    context=locals()
    template = 'pbx_settings.html'
    return render(request, template, context)