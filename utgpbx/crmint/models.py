# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
# Create your models here.
from utgusers.models import AuthUser


class Crm(models.Model):
    id = models.BigIntegerField(primary_key=True)
    crm_name = models.CharField(max_length=255, blank=True, null=True)
    contact_match_link = models.CharField(max_length=2500, blank=True, null=True)
    open_contact_link = models.CharField(max_length=2500, blank=True, null=True)
    create_contact_link = models.CharField(max_length=2500, blank=True, null=True)
    journeling_link = models.CharField(max_length=2500, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'crm'


class UserCrm(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING, primary_key=True)
    crm = models.ForeignKey(Crm, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'user_crm'
        unique_together = (('user', 'crm'),)