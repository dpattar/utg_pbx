# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.shortcuts import render
# Create your views here.
from crmint.add_crm_form import CrmAddForm
from crmint.models import Crm, UserCrm
from django.db.models import Max
from utgusers.models import AuthUser


@login_required(login_url='/login/')
def crm_view(request, *args, **kwargs):
    contat_obj=UserCrm.objects.select_related()
    context=locals()
    template = 'view_crm.html'
    return render(request, template, {'contat_obj' : contat_obj})


@login_required(login_url='/login/')
def add_crm_view(request, *args, **kwargs):
    ids = Crm.objects.all().aggregate(Max('id'))
    id = ids.get('id', 1)
    title = "Add"
    if request.method == 'POST':
        form = CrmAddForm(request.POST)
        if form.is_valid():
            # form.save(commit=True)
            crm_name = form.cleaned_data.get("crm_name")
            contact_match_link = form.cleaned_data.get('contact_match_link')
            open_contact_link = form.cleaned_data.get('open_contact_link')
            create_contact_link = form.cleaned_data.get('create_contact_link')
            journeling_link = form.cleaned_data.get('journeling_link')

            usr = Crm()
            usr.id=id+1
            usr.crm_name = crm_name
            usr.contact_match_link = contact_match_link
            usr.open_contact_link = open_contact_link
            usr.create_contact_link = create_contact_link
            usr.journeling_link = journeling_link
            usr.save()

            return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
            # return render(request, 'utgusers.html', {})
            # form.save()
        else:
            print form.errors
    else:
        form = CrmAddForm()
    template = 'add_crm.html'
    return render(request, template, {"form": form, "title": title})