from django import forms
from contacts.models import Contacts

class CrmAddForm(forms.Form):
    crm_name = forms.CharField(max_length=255)
    crm_name.widget.attrs.update({'class': 'form-control'})

    contact_match_link = forms.CharField(max_length=255)
    contact_match_link.widget.attrs.update({'class': 'form-control'})

    open_contact_link = forms.CharField(max_length=255)
    open_contact_link.widget.attrs.update({'class': 'form-control'})

    create_contact_link = forms.CharField(max_length=255)
    create_contact_link.widget.attrs.update({'class': 'form-control'})

    journeling_link = forms.CharField(max_length=255)
    journeling_link.widget.attrs.update({'class': 'form-control'})

