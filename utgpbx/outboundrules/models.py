# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class OutboundRules(models.Model):
    id = models.BigIntegerField(primary_key=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    call_from = models.BigIntegerField(blank=True, null=True)
    prefix = models.BigIntegerField(blank=True, null=True)
    ext_group = models.BigIntegerField(blank=True, null=True)
    route1 = models.BigIntegerField(blank=True, null=True)
    route2 = models.BigIntegerField(blank=True, null=True)
    route3 = models.BigIntegerField(blank=True, null=True)
    route4 = models.BigIntegerField(blank=True, null=True)
    route5 = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'outbound_rules'