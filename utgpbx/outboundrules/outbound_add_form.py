from django import forms

from outboundrules.models import OutboundRules
from siptrunks.models import SipTrunks

class OutboundAddForm(forms.ModelForm):


    name = forms.CharField(max_length=255)
    name.widget.attrs.update({'class': 'form-control'})

    call_from = forms.CharField(max_length=255)
    call_from.widget.attrs.update({'class': 'form-control'})

    prefix = forms.CharField(max_length=255)
    prefix.widget.attrs.update({'class': 'form-control'})

    ext_group = forms.CharField(max_length=255)
    ext_group.widget.attrs.update({'class': 'form-control'})

    class Meta:
        model = OutboundRules
        fields = ['name', 'call_from', 'prefix', 'ext_group']
