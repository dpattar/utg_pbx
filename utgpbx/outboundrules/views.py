# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.template import RequestContext, Context, loader

# Create your views here.
from outboundrules.models import OutboundRules
from outboundrules.outbound_add_form import OutboundAddForm

from django.db.models import Max
from django.contrib.auth.decorators import login_required

@login_required(login_url='/login/')
def outbound_rules_view(request, *args, **kwargs):
    out_obj=OutboundRules.objects.all()
    context=locals()
    template = 'outbound_rules.html'
    return render(request, template, {'out_obj' : out_obj})

def utg_outbound_delete(request, id, *args, **kwargs):
    object_users_model1 = OutboundRules.objects.get(id=id)
    object_users_model1.delete()
    object_users_model = OutboundRules.objects.all()
    template = 'utgusers.html'
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))

@login_required(login_url='/login/')
def add_outbound_rules_view(request, *args, **kwargs):
        ids = OutboundRules.objects.all().aggregate(Max('id'))
        id = ids.get('id', 1)
        title = "Add"
        if request.method == 'POST':
            form = OutboundAddForm(request.POST)
            if form.is_valid():
                # form.save(commit=True)
                name = form.cleaned_data.get("name")
                call_from = form.cleaned_data.get('call_from')
                prefix = form.cleaned_data.get('prefix')
                ext_group = form.cleaned_data.get('ext_group')


                usr = OutboundRules()
                usr.id = id
                usr.name = name
                usr.call_from = call_from
                usr.prefix = prefix
                usr.ext_group = ext_group
                obj = {'name': name, 'call_from': call_from, 'prefix': prefix,'ext_group':ext_group}
                usr.save()
                connect_outbound_rules_view(request, name,obj)
                return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
                #return render(request, 'utgusers.html', {})
                # form.save()
            else:
                print form.errors
        else:
            form = OutboundAddForm()
        template = 'add_outbound_rule.html'
        return render(request, template, {"form": form, "title": title})


@login_required(login_url='/login/')
def edit_outbound_rules_view(request,id, *args, **kwargs):

        title = "Add"
        if request.method == 'POST':
            form = OutboundAddForm(request.POST)
            if form.is_valid():
                # form.save(commit=True)
                name = form.cleaned_data.get("name")
                call_from = form.cleaned_data.get('call_from')
                prefix = form.cleaned_data.get('prefix')
                ext_group = form.cleaned_data.get('ext_group')

                usr = OutboundRules.objects.get(id=id)
                usr.id = id
                usr.name = name
                usr.call_from = call_from
                usr.prefix = prefix
                usr.ext_group = ext_group

                usr.save()

                return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
                #return render(request, 'utgusers.html', {})
                # form.save()
            else:
                print form.errors
        else:
            out_model = OutboundRules.objects.get(id=id)
            form = OutboundAddForm(instance=out_model)

        template = 'add_outbound_rule.html'
        return render(request, template, {"form": form, "title": title})


def connect_outbound_rules_view(request, name,obj):
   try:
        t = loader.get_template('sip.xml')
   except :
       print(request, """customer sip config xml file update failed.
                   Can not load template file !""")

   try:
       path=r'/usr/local/UTG/conf/dialplan/default/'+name+'.xml'
       f= open(path,'w')
       try:
           f.write(t.render(obj))
           f.close()
       except:
           print(request, """customer sip config xml file update failed.
                     Can not load template file !""")
   finally:
       print(request, """customer sip config xml file update failed.
                            Can not load template file !""")


   return True