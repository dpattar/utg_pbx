
from audiofield.widgets import CustomerAudioFileWidget
from django.forms import forms, ModelForm
from recordings.models import Recordings


class CustomerRecordingFileForm(ModelForm):
    # file_name = forms.CharField(required=True, max_length=150)
    # file_name.widget.attrs.update({'class': 'form-control'})

    audio_file = forms.FileField(widget=CustomerAudioFileWidget)

    class Meta:
        model = Recordings
        fields = ['file_name','audio_file' ]

