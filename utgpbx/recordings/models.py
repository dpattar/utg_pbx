# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf import settings
from audiofield.fields import AudioField
import os.path

from django.db import models

# Create your models here.
class Recordings(models.Model):
    id = models.BigIntegerField(primary_key=True)
    date = models.DateField(blank=True, null=True)
    caller_id = models.BigIntegerField(blank=True, null=True)
    display_name = models.CharField(max_length=255, blank=True, null=True)
    source = models.BigIntegerField(blank=True, null=True)
    destination = models.BigIntegerField(blank=True, null=True)
    file_name = models.CharField(max_length=255, blank=True, null=True)
    # Add the audio field to your model
    audio_file = AudioField(upload_to='Users/dayanandpattar/Documents', blank=True,
                            ext_whitelist=(".mp3", ".wav", ".ogg"),
                            help_text=("Allowed type - .mp3, .wav, .ogg"))

    # Add this method to your model
    def audio_file_player(self):
        """audio player tag for admin"""
        if self.audio_file:
            file_url = settings.MEDIA_URL + str(self.audio_file)
            player_string = '<audio src="%s" controls>Your browser does not support the audio element.</audio>' % (
            file_url)
            return player_string

    audio_file_player.allow_tags = True
    audio_file_player.short_description = ('Audio file player')

    class Meta:
        managed = False
        db_table = 'recordings'




