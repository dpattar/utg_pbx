# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os

from audiofield.widgets import CustomerAudioFileWidget
from django.shortcuts import render, render_to_response
from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect

# Create your views here.
#from audiofield.audiofield.widgets import CustomerAudioFileWidget
from django.template import RequestContext

# from audiofield.audiofield.widgets import CustomerAudioFileWidget
from loginpage.loginform import User
# from recordings.RecordingsForm import CustomerAudioFileForm
# from recordings.RecordingsForm import CustomerAudioFileForm
from recordings.RecordingsForm import CustomerRecordingFileForm
from recordings.models import Recordings
from django.contrib.auth.decorators import login_required
from django.views.static import serve

@login_required(login_url='/login/')
def recordings_view(request, *args, **kwargs):
   # rec_obj=Recordings.objects.all()
    #context=locals()
    #path = "C:/Program Files (x86)/UTG/recordings/"  # insert the path to your directory
    path = "/usr/local/UTG/recordings/"
    rec_obj = os.listdir(path)
    source_obj = []
    for rec_objw in rec_obj :
        str1 = "/static/"
        source_obj.append(str1+rec_objw)


    obj = zip(rec_obj,source_obj)
    template = 'recordings.html'
    return render_to_response(template, {'obj': obj})

