from django import forms
from domains.models import Domains

class DomainAddForm(forms.ModelForm):
    domain_name = forms.CharField(max_length=255)
    domain_name.widget.attrs.update({'class': 'form-control','placeholder':'Steve'})
    status = (('1', 'Enabled'), ('0', 'Disabled'))
    is_enabled = forms.ChoiceField(choices=status)
    is_enabled.widget.attrs.update({'class': 'form-control'})
    description = forms.CharField(max_length=255)
    description.widget.attrs.update({'class': 'form-control','placeholder':'Description'})
    max_connections = forms.CharField(max_length=255)
    max_connections.widget.attrs.update({'class': 'form-control','placeholder':'20'})

    class Meta:
        model = Domains
        fields = ['domain_name', 'is_enabled', 'description', 'max_connections']


