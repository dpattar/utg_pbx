# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Domains(models.Model):
    domain_name = models.CharField(primary_key=True, max_length=255)
    is_enabled = models.BooleanField()
    description = models.CharField(max_length=255, blank=True, null=True)
    max_connections = models.BigIntegerField(blank=True, null=True)
    id=models.BigIntegerField(blank=True, null=True)

    def __unicode__(self):
        return str(self.domain_name)

    class Meta:
        managed = False
        db_table = 'domains'