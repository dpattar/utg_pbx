# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.db.models import Max
# Create your views here.
from domains.add_domain_form import DomainAddForm
from domains.edit_domains import DomainEditForm
from domains.models import Domains
from django.contrib.auth.decorators import login_required

@login_required(login_url='/login/')
def domains_view(request, *args, **kwargs):
    domain_obj=Domains.objects.all()
    context=locals()
    template = 'domains.html'
    return render(request, template, {'domain_obj' : domain_obj})

@login_required(login_url='/login/')
def add_domains_view(request, *args, **kwargs):


    title = "Add"
    if request.method == 'POST':
        form = DomainAddForm(request.POST)
        if form.is_valid():
            # form.save(commit=True)
            name = form.cleaned_data.get("domain_name")
            enabled = form.cleaned_data.get("is_enabled")
            desc = form.cleaned_data.get("description")
            max = form.cleaned_data.get("max_connections")
            if enabled == '1' :
                is_enabled=True
            else:
                is_enabled=False
            usr = Domains()
            usr.domain_name = name
            usr.is_enabled = is_enabled
            usr.description = desc
            usr.max_connections= max
            usr.save()
            return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
            # return render(request, 'utgusers.html', {})
            # form.save()
        else:
            print form.errors
    else:
        form = DomainAddForm()
    template = 'add_domain.html'
    return render(request, template, {"form": form, "title": title})


def edit_domains_view(request,id, *args, **kwargs):

    title = "Add"
    if request.method == 'POST':
        form = DomainEditForm(request.POST)
        if form.is_valid():
            # form.save(commit=True)
            name = form.cleaned_data.get("domain_name")
            enabled = form.cleaned_data.get("is_enabled")
            desc = form.cleaned_data.get("description")
            max = form.cleaned_data.get("max_connections")
            if enabled == '1' :
                is_enabled=True
            else:
                is_enabled=False
            usr = Domains.objects.get(id=id)
            usr.domain_name = name
            usr.is_enabled = is_enabled
            usr.description = desc
            usr.max_connections= max

            usr.save()
            return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
            # return render(request, 'utgusers.html', {})
            # form.save()
        else:
            print form.errors
    else:
        domain_obj = Domains.objects.get(id=id)
        form = DomainEditForm(instance=domain_obj)
    template = 'edit_domain.html'
    return render(request, template, {"form": form, "title": title})

def utg_domains_delete(request,id, *args, **kwargs):
    object_users_model1 = Domains.objects.get(id=id)
    object_users_model1.delete()
    object_users_model=Domains.objects.all()
    template = 'domains.html'
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
