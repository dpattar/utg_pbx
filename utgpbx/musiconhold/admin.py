# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os.path
from django.contrib import admin
from musiconhold.models import Document
# Register your models here.

# list_display = ('audio_file_player')
# actions = ['custom_delete_selected']
#
# def custom_delete_selected(self, request, queryset):
#     #custom delete code
#     n = queryset.count()
#     for i in queryset:
#         if i.audio_file:
#             if os.path.exists(i.audio_file.path):
#                 os.remove(i.audio_file.path)
#         i.delete()
#     self.message_user(request, _("Successfully deleted %d audio files.") % n)
# custom_delete_selected.short_description = "Delete selected items"
#
# def get_actions(self, request):
#     actions = super(Document, self).get_actions(request)
#     del actions['delete_selected']
#     return actions