# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf import settings
import os.path
from django.db import models

class Document(models.Model):
    name = models.CharField(max_length=150, blank=False)
    audio_file= models.FileField(upload_to='moh_files/')


    # audio_file = AudioField(upload_to='H:/Newfolder/', blank=True, ext_whitelist=(".mp3", ".wav", ".ogg"),
    #                         verbose_name=("audio file"))
    # created_date = models.DateTimeField(auto_now_add=True)
    # updated_date = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = u'audio_file'

    # def audio_file_player(self):
    #     """audio player tag for admin"""
    #     if self.audio_file:
    #         file_url = settings.MEDIA_URL + str(self.audio_file)
    #         player_string = '<audio controls><source src="%s" >Your browser does not support the audio element.</audio>' % \
    #                         (file_url)
    #         return player_string
    #
    # audio_file_player.allow_tags = True
    # audio_file_player.short_description = ('audio file player')


