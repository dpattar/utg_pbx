from django import forms
from musiconhold.models import Document


class MohAddForm(forms.ModelForm):

    audio_file = forms.FileField()

    class Meta :
        model = Document
        fields = ['name','audio_file']
        exclude = ('name',)




