# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.template import RequestContext
from django.http import HttpResponseRedirect

from musiconhold.add_moh_form import MohAddForm
from  .models import Document




# Create your views here.
@login_required(login_url='/login/')
def moh_view(request, *args, **kwargs):
    template = 'moh.html'
    form = MohAddForm()
    title="name"

    # Add audio
    if request.method == 'POST':
        form = MohAddForm(request.POST, request.FILES)
        if form.is_valid():
            if form.save():
                return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
            else:
                return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/dashboard/'))
            # obj = form.save(commit=False)
            # obj.save()
           # handle_uploaded_file
           #  if request.FILES.get('audio_file'):
           #      myfile = request.FILES.get('audio_file')
           #  else:
           #      myfile = None
           #  if save_file(myfile):
           #     return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
           #  else:
           #     return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/dashboard/'))

            # handle_uploaded_file(request.FILES['file'])



        # To retain frontend widget, if form.is_valid() == False
        # form.fields['audio_file'].widget = CustomerAudioFileWidget()
    else:
        form = MohAddForm()  # A empty, unbound form
    template = 'moh.html'
    return render(request,template,  {"form": form, "title": title})


    # if request.method == 'POST':
    #     form = MohAddForm(request.POST, request.FILES)
    #     if form.is_multipart():
    #         # newdoc = Document(docfile=request.FILES['file'])
    #         # newdoc.save()
    #         handle_uploaded_file(request.FILES['file'])
    #
    #         return HttpResponseRedirect('Success')
    # else:
    #     form = MohAddForm()  # A empty, unbound form
    #
    #     # Load documents for the list page
    # documents = Document.objects.all()
    #
    # # Render list page with the documents and the form
    # return render(
    #     request,
    #     'common_audiofield.html',
    #     {'documents': documents, 'form': form}
    # )

def save_file(file, path=''):
    ''' Little helper to save a file
    '''
    filename = file._get_name()
    fd = open('%s/%s' % ('H:/Newfolder'+filename), 'wb')
    for chunk in file.chunks():
        fd.write(chunk)
    fd.close()

def handle_uploaded_file(f):
    with open('H:/Newfolder/name.txt', 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)