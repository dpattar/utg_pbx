# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.decorators import login_required
from django.shortcuts import render

# Create your views here.
@login_required(login_url='/login/')
def settings_view(request, *args, **kwargs):
    context=locals()
    template = 'appsettings.html'
    return render(request, template, context)