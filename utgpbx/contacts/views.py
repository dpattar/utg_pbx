# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponseRedirect
from django.shortcuts import render

# Create your views here.
from contacts.add_contacts_form import ContactAddForm
from contacts.models import Contacts
from django.db.models import Max
from django.contrib.auth.decorators import login_required

@login_required(login_url='/login/')
def contacts_view(request, *args, **kwargs):
    contat_obj=Contacts.objects.all()
    context=locals()
    template = 'contacts.html'
    return render(request, template, {'contat_obj' : contat_obj})

def utg_contact_delete(request, id, *args, **kwargs):
    object_users_model1 = Contacts.objects.get(id=id)
    object_users_model1.delete()
    object_users_model = Contacts.objects.all()
    template = 'utgusers.html'
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))

@login_required(login_url='/login/')
def add_contacts_view(request, *args, **kwargs):
    ids = Contacts.objects.all().aggregate(Max('id'))
    id = ids.get('id', 1)
    title = "Add"
    if request.method == 'POST':
        form = ContactAddForm(request.POST)
        if form.is_valid():
            # form.save(commit=True)
            first_name = form.cleaned_data.get("first_name")
            last_name = form.cleaned_data.get('last_name')
            company = form.cleaned_data.get('company')
            business = form.cleaned_data.get('business')
            email = form.cleaned_data.get('email')
            mobile = form.cleaned_data.get('mobile')
            password = form.cleaned_data.get('password')
            ip_address = form.cleaned_data.get('ip_address')
            mac_address = form.cleaned_data.get('mac_address')

            usr = Contacts()
            usr.id=id+1
            usr.first_name = first_name
            usr.last_name = last_name
            usr.company = company
            usr.business = business
            usr.email = email
            usr.mobile = mobile
            usr.password = password
            usr.ip_address = ip_address
            usr.mac_address = mac_address
            usr.save()

            return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
            # return render(request, 'utgusers.html', {})
            # form.save()
        else:
            print form.errors
    else:
        form = ContactAddForm()
    template = 'add_contacts.html'
    return render(request, template, {"form": form, "title": title})


@login_required(login_url='/login/')
def edit_contacts_view(request,id, *args, **kwargs):

    title = "Add"
    if request.method == 'POST':
        form = ContactAddForm(request.POST)
        if form.is_valid():
            # form.save(commit=True)
            first_name = form.cleaned_data.get("first_name")
            last_name = form.cleaned_data.get('last_name')
            company = form.cleaned_data.get('company')
            business = form.cleaned_data.get('business')
            email = form.cleaned_data.get('email')
            mobile = form.cleaned_data.get('mobile')
            password = form.cleaned_data.get('password')
            ip_address = form.cleaned_data.get('ip_address')
            mac_address = form.cleaned_data.get('mac_address')

            usr = Contacts.objects.get(id=id)
            usr.first_name = first_name
            usr.last_name = last_name
            usr.company = company
            usr.business = business
            usr.email = email
            usr.mobile = mobile
            usr.password = password
            usr.ip_address = ip_address
            usr.mac_address = mac_address
            usr.save()

            return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
            # return render(request, 'utgusers.html', {})
            # form.save()
        else:
            print form.errors
    else:
        object_users_model = Contacts.objects.get(id=id)
        form = ContactAddForm(instance=object_users_model)
    template = 'add_contacts.html'
    return render(request, template, {"form": form, "title": title})