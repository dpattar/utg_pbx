from django import forms
from contacts.models import Contacts

class ContactAddForm(forms.ModelForm):
    first_name = forms.CharField(max_length=255)
    first_name.widget.attrs.update({'class': 'form-control','placeholder':'Max'})

    last_name = forms.CharField(max_length=255)
    last_name.widget.attrs.update({'class': 'form-control','placeholder':'Jobs'})

    company = forms.CharField(max_length=255)
    company.widget.attrs.update({'class': 'form-control','placeholder':'Wyne Enterprises'})

    business = forms.CharField(max_length=255)
    business.widget.attrs.update({'class': 'form-control','placeholder':'Support'})

    email = forms.EmailField(max_length=255)
    email.widget.attrs.update({'class': 'form-control','placeholder':'example@example.com'})

    mobile = forms.CharField(max_length=255)
    mobile.widget.attrs.update({'class': 'form-control','placeholder':'9832098320'})

    class Meta:
        model = Contacts
        fields = ['first_name', 'last_name', 'company', 'business', 'email',
                  'mobile']
