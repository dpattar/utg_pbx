# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Inboundrules(models.Model):
    id = models.BigIntegerField(primary_key=True)
    type = models.CharField(max_length=255, blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    trunk = models.BigIntegerField(blank=True, null=True)
    did_number = models.BigIntegerField(blank=True, null=True)
    in_office_routing = models.BigIntegerField(blank=True, null=True)
    out_office_routing = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'inboundrules'