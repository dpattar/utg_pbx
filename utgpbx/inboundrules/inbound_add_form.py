from django import forms

from inboundrules.models import Inboundrules


class InboundAddForm(forms.ModelForm):
    type = forms.CharField(max_length=255)
    type.widget.attrs.update({'class': 'form-control','placeholder':'GreenLamp1'})

    name = forms.CharField(max_length=255)
    name.widget.attrs.update({'class': 'form-control'})

    trunk = forms.CharField(max_length=255)
    trunk.widget.attrs.update({'class': 'form-control'})

    did_number = forms.CharField(max_length=255)
    did_number.widget.attrs.update({'class': 'form-control'})



    class Meta:
        model = Inboundrules
        fields = ['type', 'name', 'trunk', 'did_number', 'in_office_routing','out_office_routing']