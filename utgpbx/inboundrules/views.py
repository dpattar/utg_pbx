# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.template import RequestContext, Context, loader
# Create your views here.
from inboundrules.inbound_add_form import InboundAddForm
from inboundrules.models import Inboundrules
from django.db.models import Max
from django.contrib.auth.decorators import login_required

@login_required(login_url='/login/')
def inbound_rules_view(request, *args, **kwargs):
    inbound_obj=Inboundrules.objects.all()
    context=locals()
    template = 'inbound_rules.html'
    return render(request, template, {'inbound_obj' : inbound_obj })

def utg_inbound_delete(request, id, *args, **kwargs):
    object_users_model1 = Inboundrules.objects.get(id=id)
    object_users_model1.delete()
    object_users_model = Inboundrules.objects.all()
    template = 'utgusers.html'
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))

@login_required(login_url='/login/')
def add_inbound_rules_view(request, *args, **kwargs):
    ids = Inboundrules.objects.all().aggregate(Max('id'))
    id = ids.get('id', 1)
    title = "Add"
    if request.method == 'POST':
        form = InboundAddForm(request.POST)
        if form.is_valid():
            # form.save(commit=True)
            type = form.cleaned_data.get("type")
            name = form.cleaned_data.get('name')
            trunk = form.cleaned_data.get('trunk')
            did_number = form.cleaned_data.get('did_number')
            in_office_routing = form.cleaned_data.get('in_office_routing')
            out_office_routing = form.cleaned_data.get('out_office_routing')

            usr = Inboundrules()
            usr.id = id
            usr.type = type
            usr.name = name
            usr.trunk = trunk
            usr.did_number = did_number
            usr.in_office_routing = in_office_routing
            usr.out_office_routing = out_office_routing
            usr.save()
            obj = {'name': name, 'trunk': trunk, 'did_number': did_number}
            connect_inbound_rules_view(request, name,obj)
            return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
            # return render(request, 'utgusers.html', {})
            # form.save()
        else:
            print form.errors
    else:
        form = InboundAddForm()
    template = 'add_inbound_rule.html'
    return render(request, template, {"form": form, "title": title})

@login_required(login_url='/login/')
def edit_inbound_rules_view(request,id, *args, **kwargs):

    title = "Add"
    if request.method == 'POST':
        form = InboundAddForm(request.POST)
        if form.is_valid():
            # form.save(commit=True)
            type = form.cleaned_data.get("type")
            name = form.cleaned_data.get('name')
            trunk = form.cleaned_data.get('trunk')
            did_number = form.cleaned_data.get('did_number')
            in_office_routing = form.cleaned_data.get('in_office_routing')
            out_office_routing = form.cleaned_data.get('out_office_routing')

            usr = Inboundrules.objects.get(id=id)
            usr.type = type
            usr.name = name
            usr.trunk = trunk
            usr.did_number = did_number
            usr.in_office_routing = in_office_routing
            usr.out_office_routing = out_office_routing
            usr.save()

            return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
            # return render(request, 'utgusers.html', {})
            # form.save()
        else:
            print form.errors
    else:
        inbound_model = Inboundrules.objects.get(id=id)
        form = InboundAddForm(instance=inbound_model)
    template = 'add_inbound_rule.html'
    return render(request, template, {"form": form, "title": title})


def connect_inbound_rules_view(request, name, obj):
    try:
        t = loader.get_template('inbound_rules.xml')
    except:
        print(request, """customer extension config xml file update failed.
                   Can not load template file !""")

    try:
        path = '/usr/local/UTG/conf/dialplan/default/' + name + '.xml'
        f = open(path, 'w')
        try:
            f.write(t.render(obj))
            f.close()
        except:
            print(request, """customer sip config xml file update failed.
                     Can not load template file !""")
    finally:
        print(request, """customer sip config xml file update failed.
                            Can not load template file !""")

    return True
