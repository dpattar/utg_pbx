
from django.shortcuts import render_to_response
from django.template import RequestContext, Context
from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.http import HttpResponse

from datetime import datetime, timedelta
import psutil
import logging
import os
import platform
import socket
import time

from eslswitch import psdash


def server_status_view(request):
    sysinfo = psdash.get_sysinfo()
    uptime = timedelta(seconds=sysinfo['uptime'])
    uptime = str(uptime).split('.')[0]
    memory = psdash.get_memory()
    mem_wo_c = memory['total'] - memory['available']
    #netifs = psdash.get_network_interfaces().values()
   # net_interfaces = netifs  #.sort(key=lambda x: x.get('bytes_sent'), reverse=True)
    os = sysinfo['os'].decode('utf-8')
    hostname = sysinfo['hostname'].decode('utf-8')
    uptime = uptime
    # load_avg = sysinfo['load_avg']
    num_cpus = sysinfo['num_cpus']
    swap = psdash.get_swap_space()
    disks = psdash.get_disks()
    cpu = psdash.get_cpu()
    users = psdash.get_users()
    return ({'sysinfo':sysinfo,'swap':swap,'disks':disks,'cpu':cpu})