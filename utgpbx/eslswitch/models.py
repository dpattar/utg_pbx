# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.db.models import permalink, Sum, Avg, Count, Max, Min
from django.core.validators import EMPTY_VALUES
from django.core.exceptions import ValidationError
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericRelation
from django.utils.html import format_html
import datetime
import decimal
import math
from netaddr import IPNetwork, AddrFormatError
import re
from django.db import models
from django.utils.translation import ugettext_lazy as _


class VoipSwitch(models.Model):
    """ VoipSwitch Profile """
    name = models.CharField(_(u"Switch name"),
                            max_length=50,
                            help_text=_(u"Switch name"))
    ip = models.CharField(_(u"switch IP"),
                          max_length=100,
                          default="auto",
                          help_text=_(u"Switch IP."))
    esl_listen_ip = models.CharField(_(u"event socket switch IP"),
                                     max_length=100,
                                     default="127.0.0.1",
                                     help_text=_(u"Event socket switch IP."))
    esl_listen_port = models.PositiveIntegerField(_(u"""event socket switch
                                                    port"""),
                                                  default="8021",
                                                  help_text=_(u"""Event socket
                                                              switch port."""))
    esl_password = models.CharField(_(u"event socket switch password"),
                                    max_length=30,
                                    default="ClueCon",
                                    help_text=_(u"""Event socket switch
                                                password."""))
    date_added = models.DateTimeField(_(u'date added'),
                                      auto_now_add=True)
    date_modified = models.DateTimeField(_(u'date modified'),
                                         auto_now=True)

    class Meta:
        db_table = 'voip_switch'
        ordering = ('name', )
        verbose_name = _(u'VoIP Switch')
        verbose_name_plural = _(u'VoIP Switches')

    def __unicode__(self):
        return u"%s (:%s)" % (self.name, self.ip)

# SOFIA


class SipProfile(models.Model):
    """ Sofia Sip profile """
    name = models.CharField(_(u"SIP profile name"),
                            max_length=50,
                            unique=True,
                            help_text=_(u"""E.g.: the name you want ..."""))
    user_agent = models.CharField(_(u"User agent name"),
                                  max_length=50,
                                  default="pyfreebilling",
                                  help_text=_(u"""E.g.: the user agent
                                              you want ... - take care
                                              with certain characters
                                              such as @ could cause others sip
                                              proxies reject yours messages as
                                              invalid ! """))
    ext_rtp_ip = models.CharField(_(u"external RTP IP"),
                                  max_length=100,
                                  default="auto",
                                  help_text=_(u"""External/public IP
                                    address to bind to for RTP."""))
    ext_sip_ip = models.CharField(_(u"external SIP IP"),
                                  max_length=100,
                                  default="auto",
                                  help_text=_(u"""External/public IP
                                              address to bind to for
                                              SIP."""))
    rtp_ip = models.CharField(_(u"RTP IP"),
                              max_length=100,
                              default="auto",
                              help_text=_(u"""Internal IP address to bind
                                          to for RTP."""))
    sip_ip = models.CharField(_(u"SIP IP"),
                              max_length=100,
                              default="auto",
                              help_text=_(u"""Internal IP address to bind
                                          to for SIP."""))
    sip_port = models.PositiveIntegerField(_(u"SIP port"),
                                           default=5060)
    disable_transcoding = models.BooleanField(_(u"disable transcoding"),
                                              default=True,
                                              help_text=_(u"""If true, you
                                                          can not use
                                                          transcoding."""))
    accept_blind_reg = models.BooleanField(_(u"accept blind registration"),
                                           default=False,
                                           help_text=_(u"""If true, anyone can
                                                       register to the server
                                                       and will not be
                                                       challenged for
                                                       username/password
                                                       information."""))
    disable_register = models.BooleanField(_(u"disable register"),
                                           default=True,
                                           help_text=_(u"""disable register
                                                       which may be undesirable
                                                       in a public switch """))
    apply_inbound_acl = models.BooleanField(_(u"Apply an inbound ACL"),
                                            default=True,
                                            help_text=_(u"""If true, FS will
                                                      apply the default acl
                                                      list : domains """))
    auth_calls = models.BooleanField(_(u"authenticate calls"),
                                     default=True,
                                     help_text=_(u"""If true, FreeeSWITCH will
                                                 authorize all calls on this
                                                 profile, i.e. challenge the
                                                 other side for
                                                 username/password information.
                                                 """))
    log_auth_failures = models.BooleanField(_(u"log auth failures"),
                                            default=False,
                                            help_text=_(u"""It true, log
                                                      authentication failures.
                                                      Required for Fail2ban.
                                                      """))
    MULTIPLE_CODECS_CHOICES = (
        ("PCMA,PCMU,G729", _(u"PCMA,PCMU,G729")),
        ("PCMU,PCMA,G729", _(u"PCMU,PCMA,G729")),
        ("G729,PCMA,PCMU", _(u"G729,PCMA,PCMU")),
        ("G729,PCMU,PCMA", _(u"G729,PCMU,PCMA")),
        ("PCMA,G729", _(u"PCMA,G729")),
        ("PCMU,G729", _(u"PCMU,G729")),
        ("G729,PCMA", _(u"G729,PCMA")),
        ("G729,PCMU", _(u"G729,PCMU")),
        ("PCMA,PCMU", _(u"PCMA,PCMU")),
        ("PCMU,PCMA", _(u"PCMU,PCMA")),
        ("G722,PCMA,PCMU", _(u"G722,PCMA,PCMU")),
        ("G722,PCMU,PCMA", _(u"G722,PCMU,PCMA")),
        ("G722", _(u"G722")),
        ("G729", _(u"G729")),
        ("PCMU", _(u"PCMU")),
        ("PCMA", _(u"PCMA")),
        ("ALL", _(u"ALL")),
    )
    inbound_codec_prefs = models.CharField(_(u"inbound codec prefs"),
                                           max_length=100,
                                           choices=MULTIPLE_CODECS_CHOICES,
                                           default="G729,PCMU,PCMA",
                                           help_text=_(u"""Define allowed
                                                       preferred codecs for
                                                       inbound calls."""))
    outbound_codec_prefs = models.CharField(_(u"outbound codec prefs"),
                                            max_length=100,
                                            choices=MULTIPLE_CODECS_CHOICES,
                                            default="G729,PCMU,PCMA",
                                            help_text=_(u"""Define allowed
                                                        preferred codecs for
                                                        outbound calls."""))
    aggressive_nat_detection = models.BooleanField(_(u"""Agressive NAT
                                                     detection"""),
                                                   default=False,
                                                   help_text=_(u"""This will
                                                               enable NAT mode
                                                               if the network
                                                               IP/port from
                                                               which therequest
                                                               was received
                                                               differs from the
                                                               IP/Port
                                                               combination in
                                                               the SIP Via:
                                                               header, or if
                                                               the Via: header
                                                               contains the
                                                               received
                                                               parameter"""))
    NDLB_rec_in_nat_reg_c = models.BooleanField(_(u"""NDLB received
                                                  in nat reg contact"""),
                                                default=False,
                                                help_text=_(u"""add a;received=
                                                            "ip:port"
                                                            to the contact when
                                                            replying to
                                                            register
                                                            for nat handling
                                                            """))
    NDLB_FP_CHOICES = (
        ("true", _(u"true")),
        ("safe", _(u"safe")),
    )
    NDLB_force_rport = models.CharField(_(u"""NDLB Force rport"""),
                                        max_length=10,
                                        choices=NDLB_FP_CHOICES,
                                        null=True,
                                        blank=True,
                                        default="Null",
                                        help_text=_(u"""This will force
                                                    FreeSWITCH to send
                                                    SIP responses to the
                                                    network port from
                                                    which they were received.
                                                    Use at your own risk!"""))
    NDLB_broken_auth_hash = models.BooleanField(_(u"""NDLB broken auth hash
                                                  """),
                                                default=False,
                                                help_text=_(u"""Used for when
                                                            phones respond to a
                                                            challenged ACK
                                                            with method INVITE
                                                            in the hash"""))
    enable_timer = models.BooleanField(_(u"""Enable timer"""),
                                       default=False,
                                       help_text=_(u"""This enables or disables
                                                   support for RFC 4028 SIP
                                                   Session Timers"""))
    session_timeout = models.PositiveIntegerField(_(u"""Session timeout"""),
                                                  default=1800,
                                                  help_text=_(u"""session
                                                              timers for all
                                                              call to expire
                                                              after the
                                                              specified seconds
                                                              Then it will send
                                                              another invite
                                                              --re-invite. If
                                                              not specified
                                                              defaults to 30
                                                              minutes. Some
                                                              gateways may
                                                              reject values
                                                              less than 30
                                                              minutes. This
                                                              values refers to
                                                              Session-Expires
                                                              in RFC 4028 -The
                                                              time at which
                                                              an element will
                                                              consider the
                                                              session timed
                                                              out, if no
                                                              successful
                                                              session refresh
                                                              transaction
                                                              occurs
                                                              beforehand-
                                                              """))
    rtp_rewrite_timestamps = models.BooleanField(_(u"""RTP rewrite timestamps"""),
                                       default=False,
                                       help_text=_(u"""If you don't want to pass
                                           through timestampes from 1 RTP call
                                           to another"""))
    pass_rfc2833 = models.BooleanField(_(u"""pass rfc2833"""),
                                       default=False,
                                       help_text=_(u"""pass rfc2833"""))
    date_added = models.DateTimeField(_(u'date added'),
                                      auto_now_add=True)
    date_modified = models.DateTimeField(_(u'date modified'),
                                         auto_now=True)

    class Meta:
        db_table = 'sip_profile'
        ordering = ('name', )
        unique_together = ("sip_ip", "sip_port")
        verbose_name = _(u'SIP profile')
        verbose_name_plural = _(u'SIP profiles')

    def __unicode__(self):
        return u"%s (%s:%s)" % (self.name, self.sip_ip, self.sip_port)

    def get_gateways(self):
        """Get all gateways in the system assigned to this sip profile."""
        retval = []

        for account in accounts:
            for gateway in account.sofiagateway_set.all():
                if gateway.sip_profile.id == self.id:
                    retval.append(gateway)
        return retval


class SofiaGateway(models.Model):
    name = models.CharField(_(u"name"),
                            max_length=100,
                            unique=True)
    sip_profile = models.ForeignKey('SipProfile',
                                    verbose_name=_(u"SIP profile"),
                                    help_text=_(u"""Which Sip Profile
                                        communication with this gateway will
                                        take place on."""))
    channels = models.PositiveIntegerField(_(u"channels number"),
                                           default=1,
                                           help_text=_(u"""maximum simultaneous
                                               calls allowed for this gateway.
                                               """))
    enabled = models.BooleanField(_(u"Enabled"),
                                  default=True)
    prefix = models.CharField(_(u'prefix'),
                              blank=True,
                              default='',
                              max_length=15)
    suffix = models.CharField(_(u'suffix'),
                              blank=True,
                              default='',
                              max_length=15)
    MULTIPLE_CODECS_CHOICES = (
        ("PCMA,PCMU,G729", _(u"PCMA,PCMU,G729")),
        ("PCMU,PCMA,G729", _(u"PCMU,PCMA,G729")),
        ("G729,PCMA,PCMU", _(u"G729,PCMA,PCMU")),
        ("G729,PCMU,PCMA", _(u"G729,PCMU,PCMA")),
        ("PCMA,G729", _(u"PCMA,G729")),
        ("PCMU,G729", _(u"PCMU,G729")),
        ("G729,PCMA", _(u"G729,PCMA")),
        ("G729,PCMU", _(u"G729,PCMU")),
        ("PCMA,PCMU", _(u"PCMA,PCMU")),
        ("PCMU,PCMA", _(u"PCMU,PCMA")),
        ("G722,PCMA,PCMU", _(u"G722,PCMA,PCMU")),
        ("G722,PCMU,PCMA", _(u"G722,PCMU,PCMA")),
        ("G722", _(u"G722")),
        ("G729", _(u"G729")),
        ("PCMU", _(u"PCMU")),
        ("PCMA", _(u"PCMA")),
        ("ALL", _(u"ALL")),
    )
    codec = models.CharField(_(u"Codecs"),
                              max_length=30,
                              default="ALL",
                              choices=MULTIPLE_CODECS_CHOICES,
                              help_text=_(u"""Codecs allowed - beware about
                              order, 1st has high priority """))
    username = models.CharField(_(u"username"),
                                blank=True,
                                default='',
                                max_length=35)
    password = models.CharField(_(u"password"),
                                blank=True,
                                default='',
                                max_length=35)
    register = models.BooleanField(_(u"register"),
                                   default=False)
    proxy = models.CharField(_(u"proxy"),
                             max_length=48,
                             default="",
                             help_text=_(u"IP if register is False."))
    extension = models.CharField(_(u"extension number"),
                                 max_length=50,
                                 blank=True,
                                 default="",
                                 help_text=_(u"""Extension for inbound calls.
                                     Same as username, if blank."""))
    realm = models.CharField(_(u"realm"),
                             max_length=50,
                             blank=True,
                             default="",
                             help_text=_(u"""Authentication realm. Same as
                                 gateway name, if blank."""))
    from_domain = models.CharField(_(u"from domain"),
                                   max_length=50,
                                   blank=True,
                                   default="",
                                   help_text=_(u"""Domain to use in from field.
                                       Same as realm if blank."""))
    expire_seconds = models.PositiveIntegerField(_(u"expire seconds"),
                                                 default=3600,
                                                 null=True)
    retry_seconds = models.PositiveIntegerField(_(u"retry seconds"),
                                                default=30,
                                                null=True,
                                                help_text=_(u"""How many
                                                    seconds before a retry when
                                                    a failure or timeout occurs
                                                    """))
    caller_id_in_from = models.BooleanField(_(u"caller ID in From field"),
                                            default=True,
                                            help_text=_(u"""Use the callerid of
                                                an inbound call in the from
                                                field on outbound calls via
                                                this gateway."""))
    SIP_CID_TYPE_CHOICES = (
        ('none', _(u'none')),
        ('default', _(u'default')),
        ('pid', _(u'pid')),
        ('rpid', _(u'rpid')),
    )
    sip_cid_type = models.CharField(_(u'SIP CID type'),
                                    max_length=10,
                                    choices=SIP_CID_TYPE_CHOICES,
                                    default='rpid',
                                    help_text=_(u"""Modify callerID in SDP
                                        Headers."""))
    date_added = models.DateTimeField(_(u'date added'),
                                      auto_now_add=True)
    date_modified = models.DateTimeField(_(u'date modified'),
                                         auto_now=True)

    class Meta:
        db_table = 'sofia_gateway'
        ordering = ( 'name',)
        verbose_name = _(u"Sofia gateway")
        verbose_name_plural = _(u"Sofia gateways")

    def __unicode__(self):
        return u"%s" % self.name

# Hangup Cause


class HangupCause(models.Model):
    """ Hangup Cause Model """
    code = models.PositiveIntegerField(_(u"Hangup code"),
                                       unique=True,
                                       help_text=_(u"ITU-T Q.850 Code."))
    enumeration = models.CharField(_(u"enumeration"),
                                   max_length=100,
                                   null=True,
                                   blank=True,
                                   help_text=_(u"enumeration."))
    cause = models.CharField(_(u"cause"),
                             max_length=100,
                             null=True,
                             blank=True,
                             help_text=_(u"Cause."))
    description = models.TextField(_(u'description'),
                                   blank=True)
    date_added = models.DateTimeField(_(u'date added'),
                                      auto_now_add=True)
    date_modified = models.DateTimeField(_(u'date modified'),
                                         auto_now=True)

    class Meta:
        db_table = 'hangup_cause'
        ordering = ('code',)
        verbose_name = _(u"hangupcause")
        verbose_name_plural = _(u"hangupcauses")

    def __unicode__(self):
        return u"[%s] %s" % (self.code, self.enumeration)

# CDR


class CDR(models.Model):
    """ CDR Model    """
    customer = models.CharField(_(u"customer IP address"), max_length=100, null=True, help_text=_(u"Customer IP address."))
    customer_ip = models.CharField(_(u"customer IP address"), max_length=100, null=True, help_text=_(u"Customer IP address."))
    uuid = models.CharField(_(u"UUID"), max_length=100, null=True)
    bleg_uuid = models.CharField(_(u"b leg UUID"), null=True, default="", max_length=100)
    caller_id_number = models.CharField(_(u"caller ID num"), max_length=100, null=True)
    destination_number = models.CharField(_(u"Dest. number"), max_length=100, null=True)
    chan_name = models.CharField(_(u"channel name"), max_length=100, null=True)
    start_stamp = models.DateTimeField(_(u"start time"), null=True, db_index=True)
    answered_stamp = models.DateTimeField(_(u"answered time"), null=True)
    end_stamp = models.DateTimeField(_(u"hangup time"), null=True)
    duration = models.IntegerField(_(u"global duration"), null=True)
    effectiv_duration = models.IntegerField(_(u"total duration"), null=True, help_text=_(u"Global call duration since call has been received by the switch in ms."))
    effective_duration = models.IntegerField(_(u"effective duration"), null=True, help_text=_(u"real call duration in s."))
    billsec = models.IntegerField(_(u"billed duration"), null=True, help_text=_(u"billed call duration in s."))
    read_codec = models.CharField(_(u"read codec"), max_length=20, null=True)
    write_codec = models.CharField(_(u"write codec"), max_length=20, null=True)
    hangup_cause = models.CharField(_(u"hangup cause"), max_length=50, null=True, db_index=True)
    hangup_cause_q850 = models.IntegerField(_(u"q.850"), null=True)
    gateway = models.ForeignKey(SofiaGateway, verbose_name=_(u"gateway"), null=True)
    cost_rate = models.DecimalField(_(u'buy rate'), max_digits=11, decimal_places=5, default="0", null=True)
    total_sell = models.DecimalField(_(u'total sell'), max_digits=11, decimal_places=5, default="0", null=True)
    total_cost = models.DecimalField(_(u'total cost'), max_digits=11, decimal_places=5, default="0", null=True)
    prefix = models.CharField(_(u'Prefix'), max_length=30, null=True)
    rate = models.DecimalField(_(u'sell rate'), max_digits=11, decimal_places=5, null=True)
    init_block = models.DecimalField(_(u'Connection fee'), max_digits=11, decimal_places=5, null=True)
    block_min_duration = models.IntegerField(_(u'increment'), null=True)
    sip_user_agent = models.CharField(_(u'sip user agent'), null=True, max_length=100)
    sip_rtp_rxstat = models.CharField(_(u'sip rtp rx stat'), null=True, max_length=30)
    sip_rtp_txstat = models.CharField(_(u'sip rtp tx stat'), null=True, max_length=30)
    switchname = models.CharField(_(u"switchname"), null=True, default="", max_length=100)
    switch_ipv4 = models.CharField(_(u"switch ipv4"), null=True, default="", max_length=100)
    hangup_disposition = models.CharField(_(u"hangup disposition"), null=True, default="", max_length=100)
    sip_hangup_cause = models.CharField(_(u"SIP hangup cause"), null=True, default="", max_length=100)
    sell_destination = models.CharField(_(u'sell destination'), blank=True, default='', null=True, max_length=128, db_index=True)
    cost_destination = models.CharField(_(u'cost destination'), blank=True, default='', null=True, max_length=128, db_index=True)

    class Meta:
        db_table = 'cdr'
        ordering = ('start_stamp', 'customer')
        verbose_name = _(u"CDR")
        verbose_name_plural = _(u"CDRs")

    def __unicode__(self):
        if self.start_stamp:
            return self.start_stamp
        else:
            return self.custom_alias_name



class DimDate(models.Model):
    """ Date dimension """
    date = models.DateTimeField()
    day = models.CharField(_(u'day'), max_length=2)
    day_of_week = models.CharField(_(u'day of the week'), max_length=30)
    hour = models.CharField(_(u'hour'), max_length=2, null=True, blank=True)
    month = models.CharField(_(u'month'), max_length=2)
    quarter = models.CharField(_(u'quarter'), max_length=2)
    year = models.CharField(_(u'year'), max_length=4)

    class Meta:
        db_table = 'date_dimension'
        ordering = ('date',)
        verbose_name = _(u"date dimension")
        verbose_name_plural = _(u"date dimensions")

    def __unicode__(self):
        return u"%s" % self.date
