# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models

# Create your models here.

# Create your models here.
class CallQueues(models.Model):
    id = models.BigIntegerField(primary_key=True)
    extension = models.BigIntegerField(blank=True, null=True)
    poll_strategy = models.CharField(max_length=255, blank=True, null=True)
    timeout = models.BigIntegerField(blank=True, null=True)
    queue_name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'call_queues'


class AgentDetails(models.Model):
    agent = models.ForeignKey('AuthUser', models.DO_NOTHING)
    tier_level = models.BigIntegerField(blank=True, null=True)
    tier_posotion = models.BigIntegerField(blank=True, null=True)
    queue_name = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'agent_details'

class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'

    def __unicode__(self):
        return str (self.name)

class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.NullBooleanField()
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=254)
    is_admin = models.NullBooleanField()
    is_active = models.NullBooleanField()
    date_joined = models.DateTimeField(blank=True, null=True)
    is_user = models.NullBooleanField()
    is_agent = models.NullBooleanField()
    is_staff = models.NullBooleanField()
    is_enabled = models.NullBooleanField()
    user_group = models.IntegerField(blank=True, null=True)
    domain = models.CharField(max_length=254)
    outbound_caller_id = models.BigIntegerField(blank=True, null=True)
    mobile = models.BigIntegerField(blank=True, null=True)
    username = models.BigIntegerField(unique=True)
    call_queue = models.BigIntegerField(blank=True, null=True)

    def __unicode__(self):
        return '%s' % (self.username)

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class Roles(models.Model):
    role_id = models.CharField(primary_key=True, max_length=10)
    role = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'roles'


class UserRoles(models.Model):
    user = models.ForeignKey('Users', models.DO_NOTHING, primary_key=True)
    role = models.ForeignKey('Roles', models.DO_NOTHING, primary_key=True)
    role_0 = models.CharField(db_column='role', max_length=50, blank=True, null=True)  # Field renamed because of name conflict.

    class Meta:
        managed = False
        db_table = 'user_roles'
        unique_together = (('user', 'role'),)


class Users(models.Model):
    user_id = models.CharField(primary_key=True, max_length=10)
    user_name = models.CharField(max_length=50)
    domain_id = models.CharField(max_length=10)
    user_enabled = models.BooleanField()
    user_email = models.CharField(max_length=150, blank=True, null=True)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150, blank=True, null=True)
    is_enabled = models.NullBooleanField()
    is_super_admin = models.NullBooleanField()
    is_admin = models.NullBooleanField()
    is_agent = models.NullBooleanField()
    is_user = models.NullBooleanField()
    password = models.CharField(max_length=36, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'users'

class Domains(models.Model):
    domain_name = models.CharField(primary_key=True, max_length=255)
    is_enabled = models.BooleanField()
    description = models.CharField(max_length=255, blank=True, null=True)
    max_connections = models.BigIntegerField(blank=True, null=True)


    class Meta:
        managed = False
        db_table = 'domains'

    def __unicode__(self):
        return str(self.domain_name)

