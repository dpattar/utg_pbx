# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.views.decorators.csrf import csrf_protect
from decimal import Context
from itertools import chain
from django.shortcuts import render,render_to_response
from django.http import HttpResponseRedirect
from django.core.mail import EmailMultiAlternatives
from domains.models import Domains
from emailsetup.models import EmailSetup
from eslswitch.esl import getReloadDialplan

from utgusers.add_user_form import UserAddForm
from utgusers.edit_user_form import UserEditForm
from django.template import RequestContext, Context, loader
from django.core.mail import send_mail, EmailMessage
from django.contrib.auth.models import User
from django.core.mail.backends.smtp import EmailBackend
import os



# Create your views here.
from utgusers.models import AuthUser, AuthGroup


def utg_users_view(request, *args, **kwargs):
    object_users_model=AuthUser.objects.all()
    template = 'utgusers.html'
    return render_to_response(template, {'obj_usr': object_users_model})

def utg_users_delete(request,username, *args, **kwargs):
    object_users_model1 = AuthUser.objects.get(username=username)
    object_users_model1.delete()
    object_users_model=AuthUser.objects.all()
    template = 'utgusers.html'
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))

def utg_edit_users_view(request, *args, **kwargs):

    title = "Add"
    if request.method== 'POST' :
        form=UserAddForm(request.POST)
        if form.is_valid():
            user_id = form.cleaned_data.get("username")
            email = form.cleaned_data.get('email')
            first_name = form.cleaned_data.get('first_name')
            last_name = form.cleaned_data.get('last_name')
            user_type = form.cleaned_data.get('usertype')
            password = form.cleaned_data.get('password')
            outbound = form.cleaned_data.get('outbound')
            mobile = form.cleaned_data.get('mobile')
            domain=form.cleaned_data.get('domain')
            #domain_name = Domains.objects.get(domain_name=domain.domain_name)
            created=User.objects.create_user(username=user_id,password=password)
            print domain
            if created :
                usr = AuthUser.objects.get(username=user_id)
                usr.email=email
                usr.first_name = first_name
                usr.last_name=last_name
                usr.is_enabled = True
                usr.user_group=user_type.id
                usr.outbound_caller_id=outbound
                usr.mobile=mobile
                usr.domain = domain.domain_name
                #usr.doamin=domain_name.domain_name
                usr.save()
                #send_mail('test', 'will mail come', 'daya6252@gmail.com', [email])


            obj={'user_id':user_id,'password':password,'effective_caller_id':outbound,'user_type':user_type }
            if connect_extensions_view(request, user_id, obj):
                if create_provision(request, user_id, obj):
                    object_email = EmailSetup.objects.first()
                    backend = EmailBackend(host=object_email.smtp_host, port=object_email.port, username=object_email.user_name, password=object_email.password)
                    subject = object_email.subject
                    from_email= object_email.from_field
                    text_content = object_email.body
                    msg = EmailMessage(subject=subject, body=text_content, from_email=from_email,to=[email], connection=backend)
                    msg.attach_file('/usr/local/src/UTG/conf/dialplans/default/' + user_id + '.utgphoneprc')
                    msg.send()
                    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
            else:
                return render(request, 'dashboard.html', {})

            #return render(request, 'utgusers.html', {})
            #form.save()
        else:
            print form.errors
    else:

        form=UserAddForm()
    template = 'add_user.html'
    return render(request,template,{"form": form, "title": title} )

def update_user_view(request,username, *args, **kwargs):
    title = "Add"
    if request.method== 'POST' :
        form=UserAddForm(request.POST)
        if form.is_valid():
            user_id = form.cleaned_data.get("username")
            email = form.cleaned_data.get('email')
            first_name = form.cleaned_data.get('first_name')
            last_name = form.cleaned_data.get('last_name')
            user_type = form.cleaned_data.get('usertype')
            password = form.cleaned_data.get('password')
            outbound = form.cleaned_data.get('outbound_caller_id')
            mobile = form.cleaned_data.get('mobile')
            domain=form.cleaned_data.get('domain')
            created=User.objects.create_user(username=user_id,password=password)
            if created :
                usr = AuthUser.objects.get(username=user_id)
                usr.email=email
                usr.first_name = first_name
                usr.last_name=last_name
                usr.is_enabled = True
                usr.user_group=user_type.id
                usr.outbound_caller_id=outbound
                usr.mobile=mobile
                usr.doamin=domain.domain_name
                usr.save()
                #send_mail('test', 'will mail come', 'daya6252@gmail.com', [email],)

            obj={'user_id':user_id,'password':password,'effective_caller_id':outbound,'user_type':user_type }
            if connect_extensions_view(request, user_id, obj):
                return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
            else:
                return render(request, 'dashboard.html', {})

            #return render(request, 'utgusers.html', {})
            #form.save()
        else:
            print form.errors
    else:
        print kwargs
        object_users_model = AuthUser.objects.get(username=username)
        form=UserAddForm(instance=object_users_model)
    template = 'add_user.html'
    return render(request,template,{"form": form, "title": title} )


def connect_extensions_view(request, name,obj):
   try:
        t = loader.get_template('extension.xml')
   except :
       print(request, """customer extension config xml file update failed.
                   Can not load template file !""")

   try:
       path='/usr/local/src/UTG/conf/directory/default/'+name+'.xml'
       f= open(path,'w')
       try:
           f.write(t.render(obj))
           f.close()
       except:
           print(request, """customer sip config xml file update failed.
                     Can not load template file !""")
   finally:
       print(request, """customer sip config xml file update failed.
                            Can not load template file !""")


   return True

def create_provision(request, name, obj):
   try:
        t = loader.get_template('hunt_group.xml')
   except :
       print(request, """customer extension config xml file update failed.
                   Can not load template file !""")

   try:
       path='/usr/local/src/UTG/conf/dialplans/default/'+name+'.utgphoneprc'
       f= open(path,'w')
       try:
           f.write(t.render(obj))
           f.close()
       except:
           print(request, """customer sip config xml file update failed.
                     Can not load template file !""")
   finally:
       print(request, """customer sip config xml file update failed.
                            Can not load template file !""")


   return True



