from django import forms

from domains.models import Domains
from utgusers.models import AuthUser, AuthGroup


class UserAddForm(forms.ModelForm):

    username = forms.CharField(required=True ,max_length=150)
    username.widget.attrs.update({'class': 'form-control'})
    username.widget.attrs.update({'type': 'number','placeholder':'1000'})
    outbound = forms.CharField(required=True ,max_length=12)
    outbound.widget.attrs.update({'class': 'form-control','placeholder':'9823418231'})
    email = forms.EmailField(max_length=255,required=False)
    email.widget.attrs.update({'class': 'form-control','placeholder':'example@example.com'})
    mobile = forms.CharField(max_length=12,required=False)
    mobile.widget.attrs.update({'class': 'form-control','placeholder':'919823191231'})
    first_name = forms.CharField(max_length=30)
    first_name.widget.attrs.update({'class': 'form-control','placeholder':'Tony Tony'})
    last_name = forms.CharField(max_length=30,required=False)
    last_name.widget.attrs.update({'class': 'form-control','placeholder':'Chopper'})
   # CHOICES=[(i, i) for i in AuthGroup._meta.get_all_field_names()]
    #usertype=forms.ChoiceField(choices=CHOICES)
    usertype = forms.ModelChoiceField(queryset=AuthGroup.objects.all(),initial=0)
    usertype.widget.attrs.update({'class': 'form-control'})
    domain = forms.ModelChoiceField(queryset=Domains.objects.all(),initial=0)
    domain.widget.attrs.update({'class': 'form-control'})
    password = forms.CharField(required=True, max_length=128, widget=forms.PasswordInput())
    password.widget.attrs.update({'class': 'form-control', 'name' : 'Password','placeholder':'*******'})

    class Meta :
        model = AuthUser
        fields = ['username','email','password', 'first_name','last_name','user_group','mobile','outbound_caller_id','domain']

