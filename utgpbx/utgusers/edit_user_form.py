from django import forms

from callqueues.models import CallQueues
from domains.models import Domains
from utgusers.models import AuthUser, AuthGroup


class UserEditForm(forms.ModelForm):
    username = forms.CharField(required=True, max_length=150)
    username.widget.attrs.update({'class': 'form-control', 'readonly': 'readonly'})
    username.widget.attrs.update({'type': 'number'})
    email = forms.CharField(max_length=255)
    email.widget.attrs.update({'class': 'form-control'})
    mobile = forms.CharField(max_length=30)
    mobile.widget.attrs.update({'class': 'form-control'})
    first_name = forms.CharField(max_length=30)
    first_name.widget.attrs.update({'class': 'form-control'})
    last_name = forms.CharField(max_length=30)
    last_name.widget.attrs.update({'class': 'form-control'})
    # CHOICES=[(i, i) for i in AuthGroup._meta.get_all_field_names()]
    # usertype=forms.ChoiceField(choices=CHOICES)
    usertype = forms.ModelChoiceField(queryset=AuthGroup.objects.all())
    usertype.widget.attrs.update({'class': 'form-control'})
    outbound_caller_id = forms.CharField(required=True, max_length=150)
    outbound_caller_id.widget.attrs.update({'class': 'form-control'})
    domain = forms.CharField(max_length=255)
    domain.widget.attrs.update({'class': 'form-control','readonly': 'readonly'})
    password = forms.CharField(required=True, max_length=128, widget=forms.PasswordInput())
    password.widget.attrs.update({'class': 'form-control', 'name': 'Password'})


    class Meta :
        model = AuthUser
        fields = ['username','email','password', 'first_name','last_name','user_group','mobile','outbound_caller_id','domain']




