# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Devices(models.Model):
    id = models.BigIntegerField(primary_key=True)
    extension = models.BigIntegerField()
    device_model = models.CharField(max_length=255, blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    user_id = models.BigIntegerField(blank=True, null=True)
    password = models.CharField(max_length=255, blank=True, null=True)
    ip_address = models.CharField(max_length=255, blank=True, null=True)
    mac_address = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'devices'

class PhoneVendors(models.Model):
    id = models.BigAutoField(primary_key=True)
    vendor = models.CharField(max_length=255, blank=True, null=True)
    model = models.CharField(max_length=255, blank=True, null=True)
    version = models.CharField(max_length=255, blank=True, null=True)

    def __unicode__(self):
        return '%s %s %s' % (self.vendor, self.model, self.version)

    def modelview(self):
        return str(self.model)

    class Meta:
        managed = False
        db_table = 'phone_vendors'