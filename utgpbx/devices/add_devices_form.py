from django import forms
from devices.models import Devices , PhoneVendors

class DeviceAddForm(forms.ModelForm):
    extension = forms.CharField(max_length=255)
    extension.widget.attrs.update({'class': 'form-control','placeholder':'1000'})

    device_model = forms.ModelChoiceField(queryset=PhoneVendors.objects.all(),initial=0)
    device_model.widget.attrs.update({'class': 'form-control'})

    name = forms.CharField(max_length=255)
    name.widget.attrs.update({'class': 'form-control','placeholder':'Bob'})

    user_id = forms.CharField(max_length=255)
    user_id.widget.attrs.update({'class': 'form-control','placeholder':'1000'})

    password = forms.CharField(max_length=255,widget=forms.PasswordInput())
    password.widget.attrs.update({'class': 'form-control'})

    ip_address = forms.CharField(max_length=255)
    ip_address.widget.attrs.update({'class': 'form-control','placeholder':'123.123.0.0'})

    mac_address = forms.CharField(max_length=255)
    mac_address.widget.attrs.update({'class': 'form-control','placeholder':'ab:c4:54:6f:rd'})

    class Meta:
        model = Devices
        fields = ['extension', 'device_model', 'name', 'user_id', 'password',
                  'ip_address', 'mac_address']

