# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.db.models import Max
# Create your views here.
from devices.add_devices_form import DeviceAddForm
from devices.models import Devices
from django.contrib.auth.decorators import login_required
from django.template import RequestContext, Context, loader


@login_required(login_url='/login/')
def devices_view(request, *args, **kwargs):
    device_obj = Devices.objects.all()
    context = locals()
    template = 'devices.html'
    return render(request, template, {'device_obj': device_obj})


def utg_device_delete(request, id, *args, **kwargs):
    object_users_model1 = Devices.objects.get(id=id)
    object_users_model1.delete()
    object_users_model = Devices.objects.all()
    template = 'utgusers.html'
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


@login_required(login_url='/login/')
def add_devices_view(request, *args, **kwargs):
    ids = Devices.objects.all().aggregate(Max('id'))
    id = ids.get('id', 1)
    title = "Add"
    if request.method == 'POST':
        form = DeviceAddForm(request.POST)
        if form.is_valid():
            # form.save(commit=True)
            extension = form.cleaned_data.get("extension")
            device_model = form.cleaned_data.get('device_model')
            name = form.cleaned_data.get('name')
            user_id = form.cleaned_data.get('user_id')
            password = form.cleaned_data.get('password')
            ip_address = form.cleaned_data.get('ip_address')
            mac_address = form.cleaned_data.get('mac_address')

            usr = Devices()
            usr.id = id + 1
            usr.extension = extension
            usr.device_model = str(device_model)
            usr.name = name
            usr.user_id = user_id
            usr.password = password
            usr.ip_address = ip_address
            usr.mac_address = mac_address
            usr.save()

            obj = {'extension': extension, 'device_model': device_model, 'name': name, 'user_id': user_id,
                   'password': password, 'ip_address': ip_address, 'mac_address': mac_address}
            return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
            # if connect_device_view(request, extension, obj):
            #     return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
            # else:
            #     return render(request, 'dashboard.html', {})
            # return render(request, 'utgusers.html', {})
            # form.save()
        else:
            print form.errors
    else:
        form = DeviceAddForm()
    template = 'add_device.html'
    return render(request, template, {"form": form, "title": title})


def update_device_view(request, id, *args, **kwargs):
    title = "Add"
    if request.method == 'POST':
        form = DeviceAddForm(request.POST)
        if form.is_valid():
            extension = form.cleaned_data.get("extension")
            device_model = form.cleaned_data.get('device_model')
            name = form.cleaned_data.get('name')
            user_id = form.cleaned_data.get('user_id')
            password = form.cleaned_data.get('password')
            ip_address = form.cleaned_data.get('ip_address')
            mac_address = form.cleaned_data.get('mac_address')

            usr = Devices.objects.get(id=id)

            usr.extension = extension
            usr.device_model = device_model
            usr.name = name
            usr.user_id = user_id
            usr.password = password
            usr.ip_address = ip_address
            usr.mac_address = mac_address
            usr.save()

            obj = {'extension': extension, 'device_model': device_model, 'name': name, 'user_id': user_id,
                   'password': password, 'ip_address': ip_address, 'mac_address': mac_address}
            if connect_device_view(request, extension, obj):
                return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
            else:
                return render(request, 'dashboard.html', {})

                # return render(request, 'utgusers.html', {})
                # form.save()
        else:
            print form.errors
    else:
        print kwargs
        object_users_model = Devices.objects.get(id=id)
        form = DeviceAddForm(instance=object_users_model)
    template = 'add_device.html'
    return render(request, template, {"form": form, "title": title})


def connect_device_view(request, name, obj):
    try:
        t = loader.get_template('phone_provisioning.xml')
    except:
        print(request, """customer extension config xml file update failed.
                   Can not load template file !""")

    try:
        path = r'usr/local/UTG/conf/dialplan/default/' + name + '.xml'
        f = open(path, 'w')
        try:
            f.write(t.render(obj))
            f.close()
        except:
            print(request, """customer sip config xml file update failed.
                     Can not load template file !""")
    finally:
        print(request, """customer sip config xml file update failed.
                            Can not load template file !""")

    return True
