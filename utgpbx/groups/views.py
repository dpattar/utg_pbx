# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
from groups.models import ExtGroups
from django.contrib.auth.decorators import login_required


@login_required(login_url='/login/')
def groups_view(request, *args, **kwargs):
    ext_grp_obj=ExtGroups.objects.all()
    context=locals()
    template = 'groups.html'
    return render(request, template, {'ext_grp_obj' : ext_grp_obj})

@login_required(login_url='/login/')
def add_groups_view(request, *args, **kwargs):
    ext_grp_obj=ExtGroups.objects.all()
    context=locals()
    template = 'add_groups.html'
    return render(request, template, {'ext_grp_obj' : ext_grp_obj})