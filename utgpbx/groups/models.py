# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class ExtGroups(models.Model):
    id = models.BigIntegerField(primary_key=True)
    group_name = models.CharField(max_length=255, blank=True, null=True)
    no_of_ext = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ext_groups'