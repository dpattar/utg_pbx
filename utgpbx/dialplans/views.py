# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from dialplans.add_dialplan_form import DialplanAddForm
from dialplans.dialplan_edit_form import DialplanEditForm
from dialplans.models import Dialplan
from django.http import HttpResponseRedirect
from django.db.models import Max
from django.contrib.auth.decorators import login_required
import os
from django.template import RequestContext, Context, loader
from utgusers.edit_user_form import UserEditForm


@login_required(login_url='/login/')
def dailplan_view(request, *args, **kwargs):
    dial_obj = Dialplan.objects.all()
    context = locals()
    template = 'dialplans.html'
    return render(request, template, {'dial_obj': dial_obj})


def dailplan_delete(request, id, *args, **kwargs):
    object_users_model1 = Dialplan.objects.get(id=id)
    object_users_model1.delete()
    object_users_model = Dialplan.objects.all()
    template = 'dialplans.html'
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


@login_required(login_url='/login/')
def add_dailplan_view(request, *args, **kwargs):
    #ids = Dialplan.objects.all().aggregate(Max('id'))
    ids = Dialplan.objects.all().latest('id')
    #idms= ids.aggregate(Max('id'))
    #id = ids.get('id', 1)
    title = "Add"
    if request.method == 'POST':
        form = DialplanAddForm(request.POST)
        if form.is_valid():
            user_id = form.cleaned_data.get("name")
            con1 = form.cleaned_data.get('condition1')
            con2 = form.cleaned_data.get('condition2')
            cv1 = form.cleaned_data.get('condition1value')
            cv2 = form.cleaned_data.get('condition2value')
            act1 = form.cleaned_data.get('action1')
            act2 = form.cleaned_data.get('action2')
            av1 = form.cleaned_data.get('actionvalue1')
            av2 = form.cleaned_data.get('actionvalue2')
            desc = form.cleaned_data.get('description')
            dilpln = Dialplan()
            dilpln.id = ids.id + 1
            dilpln.name = user_id
            dilpln.condition1 = con1.condition
            dilpln.value1 = cv1
            if con2:
                dilpln.condition2 = con2.condition
                dilpln.value2 = cv2
            dilpln.action1 = act1.action
            if av1:
                dilpln.action_value1 = av1
            if act2:
                dilpln.action1 = act2.action
                dilpln.action_value1 = av2
            dilpln.description = desc
            dilpln.save()
            if con2:
                if av1:
                    if act2:
                        obj = {'name': user_id, 'condition1': con1.condition, 'condition2': con2.condition, 'value1': cv1,
                       'value2': cv2, 'action1': act1.action, 'action2': act2.action, 'action_value1': av1,
                       'action_value2': av2}
                    else :
                        obj = {'name': user_id, 'condition1': con1.condition, 'condition2': con2.condition,
                               'value1': cv1,'value2': cv2, 'action1': act1.action,  'action_value1': av1}
                else :
                    obj = {'name': user_id, 'condition1': con1.condition, 'condition2': con2.condition, 'value1': cv1,
                           'value2': cv2, 'action1': act1.action,  'action_value1': av1}
            elif av1 :
                obj = {'name': user_id, 'condition1': con1.condition,  'value1': cv1,
                        'action1': act1.action,  'action_value1': av1
                       }
            else:
                obj = {'name': user_id, 'condition1': con1.condition,  'value1': cv1,
                        'action1': act1.action
                       }

            if connect_dialplan_view(request, user_id, obj):
                return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
            else:
                return render(request, 'dashboard.html', {})
        else:
            print form.errors
    else:

        form = DialplanAddForm()
    template = 'add_dialplan.html'
    return render(request, template, {"form": form, "title": title})


def update_dialplan_view(request, id, *args, **kwargs):
    title = "Add"
    if request.method == 'POST':
        form = DialplanEditForm(request.POST)
        if form.is_valid():
            user_id = form.cleaned_data.get("name")
            con1 = form.cleaned_data.get('condition1')
            con2 = form.cleaned_data.get('condition2')
            cv1 = form.cleaned_data.get('condition1value')
            cv2 = form.cleaned_data.get('condition2value')
            act1 = form.cleaned_data.get('action1')
            act2 = form.cleaned_data.get('action2')
            av1 = form.cleaned_data.get('actionvalue1')
            av2 = form.cleaned_data.get('actionvalue2')
            desc = form.cleaned_data.get('description')
            dilpln = Dialplan.objects.get(id=id)
            dilpln.name = user_id
            dilpln.condition1 = con1.condition
            dilpln.value1 = cv1
            if con2:
                dilpln.condition2 = con2.condition
                dilpln.value2 = cv2
            dilpln.action1 = act1.action
            if av1:
                dilpln.action_value1 = av1
            if act2:
                dilpln.action1 = act2.action
                dilpln.action_value1 = av2
            dilpln.description = desc
            dilpln.save()
            obj = {'name': user_id, 'condition1': con1.condition, 'condition2': con2.condition, 'value1': cv1,
                   'value2': cv2, 'action1': act1.action, 'action2': act2.action, 'action_value1': av1,
                   'action_value2': av1}
            if connect_dialplan_view(request, user_id, obj):
                return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
            else:
                return render(request, 'dashboard.html', {})

        else:
            print form.errors
    else:
        print kwargs
        object_users_model = Dialplan.objects.get(id=id)
        form = DialplanEditForm(instance=object_users_model)
    template = 'edit_dialplan.html'
    return render(request, template, {"form": form, "title": title})


def connect_dialplan_view(request, name, obj):
    try:
        t = loader.get_template('dialplan.xml')
    except:
        print(request, """customer extension config xml file update failed.
                   Can not load template file !""")

    try:
        path = '/usr/local/UTG/conf/dialplan/default/' + name + '.xml'
        f = open(path, 'w')
        try:
            f.write(t.render(obj))
            f.close()
        except:
            print(request, """customer sip config xml file update failed.
                     Can not load template file !""")
    finally:
        print(request, """customer sip config xml file update failed.
                            Can not load template file !""")

    return True
