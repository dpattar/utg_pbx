from django import forms
from dialplans.models import Dialplan, DialplanConditions, DialplanActions


class DialplanEditForm(forms.ModelForm):
    name = forms.CharField(max_length=255)
    name.widget.attrs.update({'class': 'form-control'})
    # conditions = (('Hour', 'Hour'), ('Day', 'Day'), ('Day_of_Month', 'Day_of_Month'), ('caller_id', 'caller_id'),('destination', 'destination'))
    condition1 = forms.ModelChoiceField(queryset=DialplanConditions.objects.all())
    condition1.widget.attrs.update({'class': 'form-control'})
    condition2 = forms.ModelChoiceField(queryset=DialplanConditions.objects.all())
    condition2.widget.attrs.update({'class': 'form-control'})
    condition1value = forms.CharField(max_length=255)
    condition1value.widget.attrs.update({'class': 'form-control'})
    condition2value = forms.CharField(max_length=255)
    condition2value.widget.attrs.update({'class': 'form-control'})
    action1 = forms.ModelChoiceField(queryset=DialplanActions.objects.all())
    action1.widget.attrs.update({'class': 'form-control'})
    action2 = forms.ModelChoiceField(queryset=DialplanActions.objects.all())
    action2.widget.attrs.update({'class': 'form-control'})
    actionvalue1 = forms.CharField(max_length=255)
    actionvalue1.widget.attrs.update({'class': 'form-control'})
    actionvalue2 = forms.CharField(max_length=255)
    actionvalue2.widget.attrs.update({'class': 'form-control'})
    description = forms.CharField(max_length=255)
    description.widget.attrs.update({'class': 'form-control'})

    class Meta:
        model = Dialplan
        fields = ['name', 'condition1', 'condition1value', 'condition2', 'condition2value', 'action1', 'action2',
                  'description', 'action_value1', 'action_value2']


