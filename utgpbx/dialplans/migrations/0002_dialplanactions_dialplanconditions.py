# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-13 09:16
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('dialplans', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='DialplanActions',
            fields=[
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
                ('action', models.CharField(blank=True, max_length=255, null=True)),
            ],
            options={
                'db_table': 'dialplan_actions',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='DialplanConditions',
            fields=[
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
                ('condition', models.CharField(blank=True, max_length=255, null=True)),
            ],
            options={
                'db_table': 'dialplan_conditions',
                'managed': False,
            },
        ),
    ]
