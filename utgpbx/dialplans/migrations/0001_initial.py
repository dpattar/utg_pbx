# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-09-30 09:11
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Dialplan',
            fields=[
                ('id', models.BigIntegerField(primary_key=True, serialize=False)),
                ('name', models.CharField(blank=True, max_length=255, null=True)),
                ('condition1', models.CharField(max_length=255)),
                ('condition2', models.CharField(blank=True, max_length=255, null=True)),
                ('action1', models.CharField(max_length=255)),
                ('action2', models.CharField(blank=True, max_length=255, null=True)),
                ('order', models.BigIntegerField(blank=True, null=True)),
                ('enabled', models.CharField(blank=True, max_length=255, null=True)),
            ],
            options={
                'db_table': 'dialplan',
                'managed': False,
            },
        ),
    ]
