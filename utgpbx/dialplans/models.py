# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Dialplan(models.Model):
    id = models.BigIntegerField(primary_key=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    condition1 = models.CharField(max_length=255)
    condition2 = models.CharField(max_length=255, blank=True, null=True)
    action1 = models.CharField(max_length=255)
    action2 = models.CharField(max_length=255, blank=True, null=True)
    order = models.BigIntegerField(blank=True, null=True)
    enabled = models.NullBooleanField()
    enabled = models.CharField(max_length=255, blank=True, null=True)
    description = models.CharField(max_length=255, blank=True, null=True)
    value1 = models.CharField(max_length=255, blank=True, null=True)
    value2 = models.CharField(max_length=255, blank=True, null=True)
    action_value1 = models.CharField(max_length=255, blank=True, null=True)
    action_value2 = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'dialplan'

class DialplanActions(models.Model):
    id = models.BigAutoField(primary_key=True)
    action = models.CharField(max_length=255, blank=True, null=True)

    def __unicode__(self):
        return str(self.action)

    class Meta:
        managed = False
        db_table = 'dialplan_actions'


class DialplanConditions(models.Model):
    id = models.BigAutoField(primary_key=True)
    condition = models.CharField(max_length=255, blank=True, null=True)

    def __unicode__(self):
        return str(self.condition)

    class Meta:
        managed = False
        db_table = 'dialplan_conditions'