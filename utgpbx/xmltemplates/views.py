# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
def xml_templates_view(request, *args, **kwargs):
    context=locals()
    template = 'xml_templates.html'
    return render(request, template, context)