# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.contrib.auth.decorators import login_required


# Create your views here.
from blocklist.models import BlaclistedTables
from calllogs.models import CallLogs
from chatlogs.models import ChatLog
from eslswitch.psdash import get_swap_space, get_cpu, get_disks, get_network_interfaces, get_memory
from eslswitch.views import server_status_view
from siptrunks.models import SipTrunks
from utgusers.models import AuthUser

@login_required(login_url='/login/')
def dashboard_view(request, *args, **kwargs):
    sip_trunks=SipTrunks.objects.count()
    extension_count=AuthUser.objects.count()
    blacklit=BlaclistedTables.objects.count()
    catlog = ChatLog.objects.count()
    call_history=CallLogs.objects.count()
    catlog=ChatLog.objects.count()
    context=locals()
    #sysinformtion =server_status_view(request)
    #network=get_network_interfaces()
    spce=get_swap_space()
    cpuus=get_cpu()
    disk=get_memory()
    # for a in spce:
    #     total=a.total
    #     used=a.used
    #     free=a.free
    #     per=a.percent
    template = 'dashboard.html'
    return render(request, template, {'sip_trunks' : sip_trunks,'extension_count':extension_count,'blacklit':blacklit,'call_history':call_history,'catlog':catlog,'spce':spce,'cpuus':cpuus,'disk':disk})

