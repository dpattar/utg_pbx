# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
import csv
import os
import glob
# Create your views here.
from calllogs.models import CallLogs
from eslswitch.models import CDR


@login_required(login_url='/login/')
def calllogs_view(request, *args, **kwargs):
    call_log_obj=CDR.objects.all()
    fromvalue=[]
    tovalue=[]
    starttime = []
    endtime = []
    duration = []
    context=locals()
    #directory = os.path.join()
    path = "/usr/local/src/UTG/log/cdr-csv/*.csv"
    for fname in glob.glob(path):
        print(fname)
        file_reader = csv.reader(open(fname), delimiter=str(u','))
        for row in file_reader:
                    # print(row)
                    fromvalue.append(row[1])
                    tovalue.append(row[2])
                    starttime.append(row[4])
                    endtime.append(row[6])
                    duration.append(row[7])
    # for root, dirs, files in os.walk("/Users/dayanandpattar/Documents/audiofiles/"):
    #     for file in files:
    #         if file.endswith(".csv"):
    #             print file
    #             #f = open(file, 'r')
    #             file_reader = csv.reader(open(file), delimiter=str(u','))
    #             #  perform calculation
    #             for row in file_reader:
    #                 # print(row)
    #                 fromvalue.append(row[1])
    #                 tovalue.append(row[2])
    #                 starttime.append(row[4])
    #                 endtime.append(row[6])
    #                 duration.append(row[7])
    #             file_reader.close()
    # file_reader = csv.reader(open('/Users/dayanandpattar/Documents/Master.csv'), delimiter=str(u','))
    # for row in file_reader:
    #     #print(row)
    #     fromvalue.append(row[1])
    #     tovalue.append(row[2])
    #     starttime.append(row[4])
    #     endtime.append(row[6])
    #     duration.append(row[7])
    template = 'call_logs.html'

    obj=zip( fromvalue,tovalue,starttime,endtime,duration)
    return render(request, template, {'obj':obj})

#
# def csv_call_logs() :
#
#     for root, dirs, files in os.walk(directory):
#         for file in files:
#             if file.endswith(".csv"):
#                 f = open(file, 'r')
#                 #  perform calculation
#                 f.close()
#
#     csv_path = "TB_data_dictionary_2014-02-26.csv"
#     file_reader = csv.reader(open('/Users/dayanandpattar/Documents/Master.csv') , delimiter=str(u','))
#     for row in file_reader:
#         # do something with row data.
#         print(row)
#
#     return True