# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class CallLogs(models.Model):
    id = models.BigIntegerField(primary_key=True)
    date_time = models.DateField(blank=True, null=True)
    from_field = models.BigIntegerField(db_column='from', blank=True, null=True)  # Field renamed because it was a Python reserved word.
    to = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'call_logs'