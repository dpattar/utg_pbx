# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.template import RequestContext, Context, loader
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
import os
from extensions.add_extension_form import ExtensionAddForm
from extensions.models import Extension
from utgusers.models import AuthUser
from django.db.models import Max

@login_required(login_url='/login/')
def extensions_view(request, *args, **kwargs):
    ext_obj=AuthUser.objects.all()
    context=locals()
    template = 'extensions.html'
    return render(request, template, {'ext_obj' : ext_obj })

@login_required(login_url='/login/')
def add_extensions_view(request, *args, **kwargs):
    ids = Extension.objects.all().aggregate(Max('id'))
    id = ids.get('id', 1)
    title = "Add"
    if request.method == 'POST':
        form = ExtensionAddForm(request.POST)
        if form.is_valid():
            # form.save(commit=True)
            extension = form.cleaned_data.get("username")
            email = form.cleaned_data.get('email')
            first_name = form.cleaned_data.get('first_name')
            last_name = form.cleaned_data.get('last_name')
            mobile = form.cleaned_data.get('usertype')
            outgoing = form.cleaned_data.get('password')
            id = form.cleaned_data.get('password')
            password = form.cleaned_data.get('password')
            webmeeting_url = form.cleaned_data.get('password')
            ext = Extension()

            ext.id = id
            return render(request, 'dashboard.html', {})
            # form.save()
        else:
            print form.errors
    else:
        form = ExtensionAddForm()
    template = 'add_extensions.html'
    return render(request, template, {"form": form, "title": title})

def add_voicemail_view(request, *args, **kwargs):
    context=locals()
    template = 'add_extensions.html'
    return render(request, template, context)

def add_forwarding_view(request, *args, **kwargs):
    context=locals()
    template = 'add_extensions.html'
    return render(request, template, context)

def add_phone_view(request, *args, **kwargs):
    context=locals()
    template = 'add_extensions.html'
    return render(request, template, context)



def connect_extensions_view(request, *args, **kwargs):
   try:
        t = loader.get_template('extension.xml')
   except :
       print(request, """customer sip config xml file update failed.
                   Can not load template file !""")
   obj_ext={'user_id':'1000','password':'1234','effective_caller_name':'1000','effective_caller_id':'1000'}
   filepath = os.path.join('/usr/local/UTG/', '1000.xml')
   try:
       f= open('H:/New folder/1002.xml','w')
       try:
           f.write(t.render(obj_ext))
           f.close()
       except:
           print(request, """customer sip config xml file update failed.
                     Can not load template file !""")
   finally:
       print(request, """customer sip config xml file update failed.
                            Can not load template file !""")

   template = 'add_extensions.html'
   return render(request, template, {})