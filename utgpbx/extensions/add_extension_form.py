from django import forms
from utgusers.models import AuthUser

class ExtensionAddForm(forms.Form):
  
    extension = forms.CharField(max_length=255)
    extension.widget.attrs.update({'class': 'form-control'})
    vendor = forms.CharField(max_length=255)
    vendor.widget.attrs.update({'class': 'form-control'})
    model = forms.CharField(max_length=255)
    model.widget.attrs.update({'class': 'form-control'})
    firmware = forms.CharField(max_length=255)
    firmware.widget.attrs.update({'class': 'form-control'})
    name = forms.CharField(max_length=255)
    name.widget.attrs.update({'class': 'form-control'})
    user_id = forms.CharField(max_length=255)
    user_id.widget.attrs.update({'class': 'form-control'})
    password = forms.CharField(max_length=255)
    password.widget.attrs.update({'class': 'form-control'})
    ip_address = forms.CharField(max_length=255)
    ip_address.widget.attrs.update({'class': 'form-control'})
    mac_address = forms.CharField(max_length=255)
    mac_address.widget.attrs.update({'class': 'form-control'})
