# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Extension(models.Model):
    id = models.BigIntegerField(primary_key=True)
    ext_no = models.BigIntegerField()
    mobile_no = models.BigIntegerField(blank=True, null=True)
    user_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'extension'