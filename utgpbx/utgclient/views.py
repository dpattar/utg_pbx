# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.contrib.auth.decorators import login_required
# Create your views here.

@login_required(login_url='/login/')
def utg_client_view(request, *args, **kwargs):
    context=locals()
    template = 'utg_client.html'
    return render(request, template, context)