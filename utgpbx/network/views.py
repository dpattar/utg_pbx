# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
def network_view(request, *args, **kwargs):
    context=locals()
    template = 'network.html'
    return render(request, template, context)