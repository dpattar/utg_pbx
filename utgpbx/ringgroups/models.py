# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class RingGroups(models.Model):
    id = models.BigIntegerField(primary_key=True)
    extension = models.BigIntegerField(blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    type = models.CharField(max_length=255, blank=True, null=True)
    if_no_ans = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ring_groups'



class RinggroupStrategy(models.Model):
    name = models.CharField(primary_key=True, max_length=255)

    class Meta:
        managed = False
        db_table = 'ringgroup_strategy'

    def __unicode__(self):
        return str(self.name)

class RinggroupUser(models.Model):
    ringgroup_id = models.BigIntegerField()
    user_id = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'ringgroup_user'