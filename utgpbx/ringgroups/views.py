# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.template import RequestContext, Context, loader
# Create your views here.
from ringgroups.models import RingGroups, RinggroupUser
from ringgroups.ringgroup_form import RinggroupAddForm
from django.db.models import Max
from django.contrib.auth.decorators import login_required
from xmlrpclib import ServerProxy

@login_required(login_url='/login/')
def ringgroups_view(request, *args, **kwargs):
    rg_obj = RingGroups.objects.all()
    context = locals()
    template = 'ring_groups.html'
    return render(request, template, {'rg_obj': rg_obj})


def utg_ringgroup_delete(request, id, *args, **kwargs):
    object_users_model1 = RingGroups.objects.get(id=id)
    # object_group_model = RinggroupUser.objects.get(ringgroup_id=id).delete()
    object_users_model1.delete()
    RinggroupUser.objects.filter(ringgroup_id=id).delete()
    # for rgp in object_group_model :
    #     rgp.delete()
    object_users_model = RingGroups.objects.all()
    template = 'ring_groups.html'
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


@login_required(login_url='/login/')
def add_ringgroups_view(request, *args, **kwargs):
    ids = RingGroups.objects.all().aggregate(Max('id'))
    id = ids.get('id', 1)

    rpc_host = 'localhost'
    rpc_username = 'freeswitch'
    rpc_password = 'works'
    rpc_port = '8080'

    title = "Add"
    if request.method == 'POST':
        form = RinggroupAddForm(request.POST)
        if form.is_valid():
            # form.save(commit=True)
            extension = form.cleaned_data.get("extension")
            name = form.cleaned_data.get('name')
            type = form.cleaned_data.get('type')
            if_no_ans = form.cleaned_data.get('if_no_ans')
            user = form.cleaned_data.get('users')
            rgroup = RingGroups()
            rgroup.id = id + 1
            rgroup.extension = extension
            rgroup.name = name
            rgroup.type = type.name
            rgroup.if_no_ans = if_no_ans
            rgroup.save()
            for usr in user:
                usrg = RinggroupUser()
                usrg.ringgroup_id = id + 1
                usrg.user_id = usr.username
                usrg.save()
            strategy=type.name
            obj = {'extension': extension, 'name': name, 'usrg': user}
            if (strategy =='Ring All') :
              create_provision1(request, name, obj)
            else :
              create_provision(request, name, obj)
            # return render(request, 'user_test.html', {"user": user})
            server = ServerProxy("http://%s:%s@%s:%s" % (rpc_username, rpc_password, rpc_host, rpc_port))
            server.freeswitch.api("reloadxml", "")
            return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
            # return render(request, 'utgusers.html', {})
            # form.save()
        else:
            print form.errors
    else:
        form = RinggroupAddForm()
    template = 'add_ring_group.html'
    return render(request, template, {"form": form, "title": title})


@login_required(login_url='/login/')
def edit_ringgroups_view(request, id, *args, **kwargs):
    title = "Add"
    if request.method == 'POST':
        form = RinggroupAddForm(request.POST)
        if form.is_valid():
            # form.save(commit=True)
            extension = form.cleaned_data.get("extension")
            name = form.cleaned_data.get('name')
            type = form.cleaned_data.get('type')
            if_no_ans = form.cleaned_data.get('if_no_ans')
            user = form.cleaned_data.get('users')
            rgroup = RingGroups()
            rgroup.extension = extension
            rgroup.name = name
            rgroup.type = type.name
            rgroup.if_no_ans = if_no_ans
            rgroup.save()
            for usr in user:
                usrg = RinggroupUser()
                usrg.ringgroup_id = id
                usrg.user_id = usr.username
                usrg.save()
            return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
            # return render(request, 'utgusers.html', {})
            # form.save()
        else:
            print form.errors
    else:
        sip_model = RingGroups.objects.get(id=id)
        # users=RinggroupUser.objects.get(ringgroup_id=id)
        form = RinggroupAddForm(instance=sip_model)

    template = 'add_ring_group.html'
    return render(request, template, {"form": form, "title": title})


def connect_ringgroup_view(request, name, obj):
    try:
        t = loader.get_template('ring_group.xml')
    except:
        print(request, """customer extension config xml file update failed.
                   Can not load template file !""")

    try:
        path = '/usr/local/src/UTG/conf/dialplans/default/' + name + '.xml'
        f = open(path, 'w')
        try:
            f.write(t.render(obj))
            f.close()
        except:
            print(request, """customer sip config xml file update failed.
                     Can not load template file !""")
    finally:
        print(request, """customer sip config xml file update failed.
                            Can not load template file !""")

    return True


def connect_huntgroup_view(request, name, obj):
    try:
        t = loader.get_template('hunt_group.xml')
    except:
        print(request, """customer extension config xml file update failed.
                   Can not load template file !""")

    try:
        path = '/usr/local/src/UTG/conf/dialplans/default/' + name + '.xml'
        f = open(path, 'w')
        try:
            f.write(t.render(obj))
            f.close()
        except:
            print(request, """customer sip config xml file update failed.
                     Can not load template file !""")
    finally:
        print(request, """customer sip config xml file update failed.
                            Can not load template file !""")

    return True


def create_provision(request, name, obj):
    try:
        t = loader.get_template('hunt_group.xml')
    except:
        print(request, """customer extension config xml file update failed.
                   Can not load template file !""")

    try:
        path = '/usr/local/src/UTG/conf/dialplans/default/' + name + '.xml'
        f = open(path, 'w')
        try:
            f.write(t.render(obj))
            f.close()
        except:
            print(request, """customer sip config xml file update failed.
                     Can not load template file !""")
    finally:
        print(request, """customer sip config xml file update failed.
                            Can not load template file !""")

    return True


def create_provision1(request, name, obj):
    print obj
    try:
        t = loader.get_template('ring_group.xml')
    except:
        print(request, """customer extension config xml file update failed.
                   Can not load template file !""")

    try:
        path = '/usr/local/src/UTG/conf/dialplans/default/' + name + '.xml'
        f = open(path, 'w')
        try:
            f.write(t.render(obj))
            f.close()
        except:
            print(request, """customer sip config xml file update failed.
                     Can not load template file !""")
    finally:
        print(request, """customer sip config xml file update failed.
                            Can not load template file !""")

    return True
