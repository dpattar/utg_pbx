from django import forms

from ringgroups.models import RingGroups, RinggroupStrategy
from siptrunks.models import SipTrunks
from utgusers.models import AuthUser




class RinggroupAddForm(forms.ModelForm):
    extension = forms.CharField(max_length=255)
    extension.widget.attrs.update({'class': 'form-control'})

    users = forms.ModelMultipleChoiceField(queryset=AuthUser.objects.all())
    users.widget.attrs.update({'class': 'form-control js-example-basic-multiple select2-container','multiple':"multiple"})

    name = forms.CharField(max_length=255)
    name.widget.attrs.update({'class': 'form-control'})

    type = forms.ModelChoiceField(queryset=RinggroupStrategy.objects.all())
    type.widget.attrs.update({'class': 'form-control'})

    if_no_ans = forms.CharField(max_length=255)
    if_no_ans.widget.attrs.update({'class': 'form-control'})

    def selected_extension_labels(self):
        return [label for value, label in self.fields['extension'].choices if value in self['extension'].value()]

    class Meta:
        model = RingGroups
        fields = ['extension', 'name', 'type', 'if_no_ans']

