# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class CallParking(models.Model):
    id = models.BigIntegerField(primary_key=True)
    no_of_spaces = models.BigIntegerField(blank=True, null=True)
    moh = models.CharField(max_length=255, blank=True, null=True)
    unpark_after = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'call_parking'