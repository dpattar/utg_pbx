# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
from callparking.models import CallParking
from django.contrib.auth.decorators import login_required


@login_required(login_url='/login/')
def callparkinge_view(request, *args, **kwargs):
    call_park_obj=CallParking.objects.all()
    context=locals()
    template = 'callparking.html'
    return render(request, template, {'call_park_obj' : call_park_obj})


def connect_dialplan_view(request, name, obj):
    try:
        t = loader.get_template('dialplan.xml')
    except:
        print(request, """customer extension config xml file update failed.
                   Can not load template file !""")

    try:
        path = '/usr/local/UTG/conf/dialplan/default/' + name + '.xml'
        f = open(path, 'w')
        try:
            f.write(t.render(obj))
            f.close()
        except:
            print(request, """customer sip config xml file update failed.
                     Can not load template file !""")
    finally:
        print(request, """customer sip config xml file update failed.
                            Can not load template file !""")

    return True