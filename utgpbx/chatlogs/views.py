# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
from chatlogs.models import ChatLog
from django.contrib.auth.decorators import login_required

@login_required(login_url='/login/')
def chatlog_view(request, *args, **kwargs):
    chat_obj=ChatLog.objects.all()
    context=locals()
    template = 'chatlogs.html'
    return render(request, template, {'chat_obj' : chat_obj})