# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class ChatLog(models.Model):
    id = models.BigIntegerField(primary_key=True)
    from_field = models.BigIntegerField(db_column='from', blank=True, null=True)  # Field renamed because it was a Python reserved word.
    to = models.BigIntegerField(blank=True, null=True)
    message = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'chat_log'