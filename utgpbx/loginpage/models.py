# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Users(models.Model):
    user_id = models.CharField(primary_key=True, max_length=10)
    username = models.CharField(max_length=50)
    domain_id = models.CharField(max_length=10)
    user_enabled = models.BooleanField()
    user_email = models.CharField(max_length=150, blank=True, null=True)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150, blank=True, null=True)
    is_enabled = models.NullBooleanField()
    is_super_admin = models.NullBooleanField()
    is_admin = models.NullBooleanField()
    is_agent = models.NullBooleanField()
    is_user = models.NullBooleanField()
    password = models.CharField(max_length=36, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'users'