from django.conf.urls import url
from . import views

app_name= 'loginpage'

urlpatterns=[
    url(r'^login_test/$',views.login_user,name='login_test'),
    url(r'^login/$',views.login_view,name='login'),
   # url(r'^dashboard/$', views.home, name='home'),
   # url(r'^logout/', views.logout_view, name='logout'),
    #url(r'^login/',views.login_view,name='login'),
]
