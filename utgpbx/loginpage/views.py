# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.models import User
from django.shortcuts import render, redirect, render_to_response
from django.contrib.auth import (
    authenticate,
    login,
    logout,
)
from django.views.decorators.csrf import csrf_exempt

from loginpage.loginform import UserLoginForm
# Create your views here.
#def login_view(request, *args, **kwargs):
 #   context=locals()
  #  template = 'login.html'
   # return render(request, template, context)



def passreset_view(request, *args, **kwargs):
    context=locals()
    template = 'password_reset.html'
    return render(request, template, context)

def logout_view(request):
    logout(request)
    title = "Login"
    form = UserLoginForm(request.POST or None)
    return render(request, "login.html", {"form": form, "title": title})

def login_view(request):
    print(request.user.is_authenticated())
    title = "Login"
    form = UserLoginForm(request.POST or None)
    if form.is_valid():
        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password')
        user = authenticate(username=username, password=password)
        login(request, user)
        print(request.user.is_authenticated())
        return redirect("/dashboard")
    return render(request, "login.html", {"form": form, "title": title})


